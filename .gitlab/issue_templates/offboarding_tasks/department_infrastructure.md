## FOR INFRASTRUCTURE TEAM MEMBERS ONLY

- [ ] Google Search Console @darawarde @sdaily: Remove the team member from Google Search Console
- [ ] @ankelly @dcouture: Remove the team member from HackerOne, if applicable
