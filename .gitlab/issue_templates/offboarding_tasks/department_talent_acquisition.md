## For Talent Acquisition only

- [ ] @MarissaFerber Remove the Team Member from ContactOut
- [ ] @MarissaFerber Remove the Team Member from Egnyte/Global Upside
- [ ] @MarissaFerber Remove the Team Member from Sterling
- [ ] @MarissaFerber Remove the Team Member from Juro
