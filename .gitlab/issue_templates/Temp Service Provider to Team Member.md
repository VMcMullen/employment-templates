**Temporary Service Provider to Team Member Issue**

Please title the issue: `[Team Member Name]` - Temp Service Provider to Team Member, Starting `[start date as team member]`

Managers, please be sure to complete this issue at **least 24 hours in advance** of the temporary service provider starting as a full time team member.

**IT Ops needs 24 hours before the team members start date to complete updates to ensure successful transer of information**

You can find the [access request issues for the Temporary Service Provider here](https://gitlab.com/gitlab-com/temporary-service-providers). 

To complete an offboarding issue, please use [this template](https://gitlab.com/gitlab-com/temporary-service-providers/lifecycle/-/blob/master/.gitlab/issue_templates/offboarding.md).

- [ ] End date of Vendor Contract `add end date here`
- [ ] Start date of new team member `add start date`
- [ ] Vendor Access Request `add AR here`
- [ ] Offboarding Vendor Issue `add issue here`


Please update the systems below to ensure the team member will be able to access the systems appropriately.

**Workday**

- [ ] PEA - [please add] `new team members GitLab email address`

**Okta**

- [ ] IT Ops - Rename Okta email and username to the new corporate email address
- [ ] IT Ops - Confirm current app assignment is converted to new logins 

**GitLab**

- [ ] IT Ops - Change Gitlab.com account to new corporate email address
- [ ] IT Ops - Remove contractor email address from account if applicable
- [ ] IT Ops - Assign `developer` permissions to Gitlab.com and Gitlab.org groups 
- [ ] IT Ops - Confirm 2FA is enabled on the Gitlab.com account
- [ ] IT Ops - Delete any pending invitiations to the Temporary Service Provider gitlab-com group and gitlab-org group.

**Google**

- [ ] IT Ops - Log on to the Google Admin console and find the Temporary Service Provider's profile.
- [ ] IT Ops - Confirm Okta converted the email address to a corporate @gitlab.com account
- [ ] IT Ops - Select the dropdown icon ˇ in the User information section and delete the email alias associated with the user.

**Slack**

- [ ] IT Ops - Disable Temporary Service Provider guest account in Slack if applicable. They will accept a Full member invite in their onboarding issue. 

**1Password**

- [ ] IT Ops - Log into 1Password and click on "People" in the right-hand sidebar. Search for the Temporary Service Provider's name. Click "More Actions" under their name and choose "Suspend" to remove access to 1Password. Take a screenshot of the user's permissions and post it as a comment in this offboarding issue.
























~/PEA Team
~/IT Ops
