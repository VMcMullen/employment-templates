#### For Application Security Only

<details>
<summary>Manager</summary>

1. [ ] Invite new team member to the essential AppSec and security department channels
    + Essential Channels
        -  [ ] `#engineering-fyi`
        -  [ ] `#g_delivery` (for security releases purposes)
        -  [ ] `#hackerone-feed`
        -  [ ] `#public_merge_requests_referencing_confidential_issues`
        -  [ ] `#sec-appsec`
        -  [ ] `#sec-appsec-standup`
        -  [ ] `#security`
        -  [ ] `#security-department`
        -  [ ] `#sec-federal-appsec` (for Federal AppSec team members)
        -  [ ] `#sec-appsec-mr-alerts`
        -  [ ] `#thanks`
1. [ ] Add the new team member to the `#hackerone-feed` username mapping so that they can receive Slack ping notifications from HackerOne mentions. [This is done via the HackerOne UI](https://docs.hackerone.com/programs/slack-integration.html#mapping-usernames)
1. [ ] Add the new team member to the Weekly Rotation spreadsheet `NamesAlphabetical` sheet with their region and potential cross-regional pair placement
</details>

<details>
<summary>All New Team Members</summary>

1. [ ] Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] Request a light agent ZenDesk account: [support handbook](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff)
1. [ ] Once you have access to Slack, join the following security related channels (ordered a-z, not by importance):
    + Optional
        -  [ ] `#infrastructure-lounge`: All things infrastructure related
        -  [ ] `#ruby-security-mailing-lists`: Ruby security mailing list updates
        -  [ ] `#sec-random`: Security team member only random chit chat (invite required)
        -  [ ] `#security-culture`: Security culture team channel
        -  [ ] `#security-lounge`: Security related chit chat
        -  [ ] `#security-infrasec`: Infrastructure Security team channel
        -  [ ] `#security-research`: Security Research team channel
        -  [ ] `#security-team-standup`: Security department standup updates
1. [ ] Familiarize yourself with the [labels](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues) we use for communicating severity and priority of security-related issues.
   1. [ ] We use our own [CVSS v3 calculator](https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/) to determine the severity of security issues. Take a look and book mark it!
1. [ ] Familiarize yourself with the [HackerOne process](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html)
1. [ ] Familiarize yourself with the other [application security runbooks](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/)
1. [ ] Follow [these steps to get a GitLab Ultimate licence](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee) which you can use on GitLab Enterprise Edition locally
1. [ ] Check out the [HackerOne program statistics](https://hackerone.com/gitlab/program_statistics) and browse some submissions from our Top Earners
1. [ ] Explore the different [engineering groups](https://about.gitlab.com/handbook/product/categories/) at GitLab and note the ones you find interesting. This will be helpful for the [Stable Counterpart](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/stable-counterparts.html) system later.
1. [ ] Check out the AppSec Weekly Triage Rotation spreadsheet (find a link to it in the `#sec-appsec` channel subject) and arrange to shadow team members on their next rotations:
    - [ ] [HackerOne](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html)
    - [ ] [Triage Rotation (mentions and issues)](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/triage-rotation.html)
1. [ ] Explore the [security department projects](https://gitlab.com/gitlab-com/gl-security) on GitLab.
1. [ ] Review [some past AppSec reviews](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues?scope=all&state=closed) and note how AppSec interacts with other team members, writes up findings, progresses them through to completion, etc
1. Setup coffee chats with one member of each [Security team](https://about.gitlab.com/handbook/engineering/security/#departmental-structure) to learn about their responsibilities how we interact with them. 
</details>
