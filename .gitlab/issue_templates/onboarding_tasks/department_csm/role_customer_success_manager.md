#### Customer Success Managers

<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new CSM team member to the current [Sales Quick Start Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop) cohort. 

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab. 
1. [ ] Notify the new member that they should be invited to a Sales Quick Start Classroom which is their Customer Success onboarding experience. 
1. [ ] Invite to sales meeting.
1. [ ] Schedule weekly 1:1 meeting.
1. [ ] Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Log in to [Tilt365](https://www.tilt365.com/) and purchase the $49 True Tilt Personality Profile (without the 2, E-Learn modules) and with the 15% coupon. See @pdaliparthi for login information and coupon code. Please use your own credit card and submit for reimbursement.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] Access Level Up (GitLab’s Learning Experience Platform) and begin working through your respective role based onboarding Journey. This Learning Path is designed to accelerate your time to productivity by focusing on the key CSM-specific elements you need to know and be able to do within the first few weeks at GitLab. Please pick from one of the following learning paths based on your role segment.
   - “[Growth and Scale Onboarding](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/scale-and-mid-touch-tam-onboarding)”. 
   - “[Strategic Onboarding](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/technical-account-manager-tam-onboarding)”.  
1. [ ] You will receive an invitation to a public Slack channel exclusively for your onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop. To learn more about the onboarding program and what to expect, take a look at the handbook page on [graduating from sales onboarding.](https://about.gitlab.com/handbook/sales/onboarding/graduating-SQS/#graduating-from-sales-onboarding) 
1. [ ] Shadow your CSM onboarding buddy on at least 4 customer calls.
1. [ ] Set up a coffee chat with Sherrod Patching, @spatching.
1. [ ] Find out what your [BCC-to-Salesforce email address](https://about.gitlab.com/handbook/customer-success/using-salesforce-within-customer-success/#tracking-emails-within-salesforce) is and save it in your addresses so that you can easily bcc it for all of your emails to customers. Optionally, you can set up a browser extension to automatically BCC this email address.
1. [ ] Request a [Light Agent account in Zendesk](https://about.gitlab.com/handbook/support/internal-support/#viewing-support-tickets).
1. [ ] In the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1YF4mtTAFmH1E7HqZASFTitGQUZubd12wsLDhzloEz3I/edit) and [Competition](https://about.gitlab.com/devops-tools/).
1. [ ] Familiarize yourself with the GitLab Sales Learning Framework listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/).
1. [ ] Review the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
1. [ ] Take the Tilt365 exam, upon receiving access (see Manager action item)

</details>
