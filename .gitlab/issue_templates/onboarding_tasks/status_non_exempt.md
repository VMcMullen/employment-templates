### Day 1 - For US Non-Exempt Team Members 

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please review GitLab's [Non-Exempt Policies & Processes](https://about.gitlab.com/handbook/finance/non-exempt/) handbook page.
    1. [ ] Please confirm that you understand whether your current role is Hourly non-exempt or Salaried non-exempt. If you are unsure, please review your Offer Letter and if you still have questions, please contact `people-compliance@gitlab.com`.
    1. [ ] New team member: Please confirm that you understand our [Meal and Rest Breaks](https://about.gitlab.com/handbook/finance/non-exempt/#meal-and-rest-breaks) policy.
    1. [ ] New team member: Please confirm that you understand our [Overtime](https://about.gitlab.com/handbook/finance/non-exempt/#overtime) policy and processes.
    1. [ ] New team member: Please confirm that you understand our [Commissions](https://about.gitlab.com/handbook/finance/non-exempt/#commissions) processes, if applicable to your role and compensation plan.
    1. [ ] New team member: Please confirm that you have received access to the ADP Time & Attendance portal.
    1. [ ] New team member: Please confirm that you are aware of your pay schedule. 
    1. [ ] New team member: Please confirm that you understand the [Timecard Reporting]() process.
    1. [ ] New team member: Complete the [ADP Time & Attendance Team Member Training]() in LevelUp.

</details>

<details>
<summary>Payroll</summary>

1. [  ] Payroll @vlaughlan, @ybasha: Ensure team member has been given access to the ADP Time & Attendance module.

</details>

