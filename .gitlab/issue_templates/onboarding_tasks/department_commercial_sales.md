#### Sales Division


<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any). Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab. 

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: Check the Sales Onboarding page of the Handbook (https://about.gitlab.com/handbook/sales/onboarding/)
1. [ ] New team member: **Please login to Level Up (Thought Industries)**  - GitLab’s Learning Experience Platform and begin working through your role based onboarding Journey “[Commercial Sales - Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/commercial-sales-onboarding-journey)”. Please begin with the “**Sales Quick Start**” section followed by the “**Required 7**” and “**Commercial Sales - First 30 Days**” This Learning Path is designed to accelerate your time to productivity by focusing on the key sales-specific elements you need to know and be able to do within the first three months at GitLab.
1. [ ] New team member: You can check the Handbook on more information on the learning path and assignments https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/ 
1. [ ] New team member: Once you complete the SQS Pre-work, attend the Sales Quick Start (SQS) In- Person Training (or Virtual Training due to COVID-19)
1. [ ] New team member: Follow the instructions and ways to use [ZoomInfo with your Salesforce Account](https://about.gitlab.com/handbook/marketing/marketing-operations/zoominfo/#about-zoominfo).
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep and SMB Customer Advocate, please consult with your manager to determine whether or not you need access to Zendesk Light Agent. If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access. Note: SMB Customer Advocates are provisioned "GitLab Staff Role" access to Zendesk and PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above. PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

</details>


<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new sales team member to the current [Sales Quick Start Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop) cohort. You will receive an invitation to a public slack channel exclusively for your onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop. To learn more about the onboarding program and what to expect, take a look at the handbook page on [graduating from sales onboarding.](https://about.gitlab.com/handbook/sales/onboarding/graduating-SQS/#graduating-from-sales-onboarding) 

</details>

