#### Channel Department

### Job-specific tasks

---
<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to the [Sales Quick Start Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop). New sales team member will receive an invitation to a public slack channel exclusively for your onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop.

</details>

<details>
<summary>Manager</summary>

1.  [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab. 
[ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Inform Sales Operations (@tav_scott @james_harrison) what territory the new team member will be working and if they have a paired SDR.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any).
Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Slack Sales Analytics (@Swetha) the new team member's quota information for upload into Salesforce.
1. [ ] Manager: Add team member to regional partner subgroup as a [maintainer](https://docs.gitlab.com/ee/user/group/)
- [ ] [GSI](https://gitlab.com/gitlab-com/channel/partners/gsi-cam-group)
- [ ] [AMER](https://gitlab.com/gitlab-com/channel/partners/amer-cam-group)
- [ ] [APAC](https://gitlab.com/gitlab-com/channel/partners/apac-cam-group)
- [ ] [EMEA](https://gitlab.com/gitlab-com/channel/partners/emea-cam-group)
- [ ] [PubSec](https://gitlab.com/gitlab-com/channel/partners/pubsec-cam-group)



</details>

<details>

<summary>New Team Member</summary>


- [ ] **Login to Level Up (Thought Industries)** and begin working through your role-based onboarding: [Channel Sales Onboarding Journey)](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/channel-sales-on-boarding). 
     - Begin with the **Sales Quick Start** section followed by the **What to know in the first 30 days section.** Sales Quickstart will help you learn things like our sales methodology, the GitLab Direction, our competitive landscape, our product, and tools like the account planning tool in Gainsight. 


</details>

<details>
<summary>Sales Ops</summary>

1. [ ] Marketing (@tav_scott): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

</details>

* Job Specific Tasks
  * PLEASE NOTE: When adding yourself to the Team Page on Day 3, please classify your department as "Channels" to appear in the correct Directory, located [HERE](https://about.gitlab.com/company/team/?department=channels). 
  * Add yourself to the following Slack Channels:
- [ ] channel-fyi
- [ ] channel-programs-ops
- [ ] channel-services
- [ ] channel-sales
- [ ] channels-emea
- [ ] channels-amer
- [ ] channel-japan-sales
- [ ] channel-team-unplugged

* Complete the following Channel Specific Onboarding Enablement:

- [ ]  Complete the Channel Sales Onboarding Pathway in [Level Up](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/channel-sales-on-boarding)
- [ ]  Request  Access to Partner Portal (ImPartner) via emailing partnerhelpdesk@gitlab.com 
- [ ]  Review [Channel Partner Program Handbook Page](https://about.gitlab.com/handbook/resellers/)
- [ ]  Review [Channel Sales Handbook Page ](https://about.gitlab.com/handbook/sales/channel/)
- [ ]  Review [Channel Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [ ]  Review [Channel Services Handbook Page](https://about.gitlab.com/handbook/resellers/services/) 
- [ ]  Review [Channel Training, Certification and Enablement Handbook Page](https://about.gitlab.com/handbook/resellers/training/) 
- [ ]  Review [GitLab Alliances Handbook Page](https://about.gitlab.com/handbook/alliances/) 
- [ ]  Review [GitLab Alliances INTERNAL ONLY Handbook Page](https://gitlab-com.gitlab.io/alliances/alliances-internal/) 
* Weeks 2 and 3 of general onboarding, schedule Coffee Chats with:
- [ ] VP WW Channels
- [ ] Senior Director Partner Enablement and Channel Programs
- [ ] Director Channel Operations
- [ ] Regional Sales Director
- [ ] Head of Channel Sales (APAC)

