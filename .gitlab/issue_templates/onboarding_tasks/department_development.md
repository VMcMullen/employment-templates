#### Engineering Division

<details>
<summary>New Team Member</summary>

##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

1. [ ] (For Engineering) Read the [Developer Onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
1. [ ] Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there, including the [development process for Security Releases](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md).
1. [ ] If you need to run the GitLab EE edition on your local environment, [request a GitLab EE developer license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) and configure it following the steps described on the [documentation page](https://docs.gitlab.com/ee/user/admin_area/license_file.html).
1. [ ] If you want [U2F security](https://about.gitlab.com/handbook/tools-and-tips/#u2f), purchase and expense for yourself a [YubiKey 5 Series](https://www.yubico.com/products/yubikey-5-overview/) or greater.
1. [ ] Set a reminder to add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) (Senior Engineer+) or code reviewer (Intermediate Engineer) for GitLab after working here for 3 months. You can do this by adding the appropriate entries to the [team page entry file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) file with which you added yourself to the team page. Here are [examples for frontend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+frontend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master), and [examples for backend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+backend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master). Doing reviews is a good way to help other team members, improve GitLab, and learn more about the code base. Because we use [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) once you add yourself as a reviewer, people will assign merge requests for you to review.
1. [ ] Ensure that you've [requested to be added to  chatops for Gitlab.com administrative tasks](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#requesting-access).
1. [ ] Try connecting to the [staging rails console via Teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/Teleport/Connect_to_Rails_Console_via_Teleport.md). This can be helpful for verifying fixes and troubleshooting issues.
1. [ ] Review the [tech stack YAML file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) to see what systems are available to you, or other team members that can help you use them.
1. [ ] Check to see that you have been added to Engineering Google Group `engineering@gitlab.com`. Google Groups and members are reported [here](https://groups.google.com/a/gitlab.com/g/engineering/members).
1. [ ] Find the recurring CTO Office Hours event located on the [GitLab Team Meeting Calendar](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar) and copy it over to your personal GitLab calendar.
1. [ ] Join the ['#engineering-fyi' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)
1. [ ] Create an [Iteration Training](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training) issue, assign it yourself, and complete the training. 
1. [ ] Create a [Feature Flag Training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=feature-flag-training) issue, assign it yourself, and complete the training. 
1. [ ] Complete the [Reliability Training in GitLab Level Up](https://levelup.gitlab.com/courses/development-reliability).
1. [ ] Read the handbook page on [Psychological Safety](https://about.gitlab.com/handbook/leadership/emotional-intelligence/psychological-safety/).

</details>

##### For Development Department

<details>
<summary>New Team Member</summary>

1. [ ] Plan to watch the [secure coding training videos](https://about.gitlab.com/handbook/engineering/security/secure-coding-training.html).  Note that they were recorded over two days.  It is suggested you break this up by topic and/or by hour over the next couple weeks.  They cover secure coding practices in general and also cover security risks and mitigations for Ruby on Rails applications.

</details>


<details>
<summary>Manager</summary>

1. [ ] If [baseline entitlements](https://gitlab.com/gitlab-com/team-member-epics/access-requests/tree/master/.gitlab/issue_templates) exist for the new team member's role, create or add to an already created access request using the appropriate template. (This may also cover some other items on this list.). If the role exists in [the access request repo](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) you can skip this task.
1. [ ] Determine if new team member will need access to the [Staging server](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging), which is used by engineers to test their changes on a Production-like environment before they land on Production. If so, create or add to an already created [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues) *with the same username the team member has on gitlab.com*.
1. [ ] Let the new team member know their account on staging.gitlab.com has been created with the same username that they have on gitlab.com.
1. [ ] Add new team member to respective [`gl-retrospectives`](https://gitlab.com/gl-retrospectives) group
1. [ ] Add new team member to [team configuration](https://gitlab.com/gitlab-org/async-retrospectives/blob/master/teams.yml) of respective `gl-retrospectives` group
1. [ ] Add new team member to your [corresponding google group](https://groups.google.com/a/gitlab.com/forum/#!myforums) (if applicable). If the new team member is a manager, give them ownership of their group.
1. [ ] Provide access to PagerDuty (if applicable).
1. [ ] (For Distribution) Provide access to the [dev-server](https://gitlab.com/gitlab-com/infrastructure/issues/1592)
1. [ ] Provide access to [Status Hero](https://statushero.com/integrations/gitlab) or [Geekbot](https://geekbot.com/) for the daily asynchronous standups (if applicable).
1. [ ] If this team member will be the DRI for one or more [tracked repositories](https://about.gitlab.com/handbook/engineering/metrics/#projects-that-are-part-of-the-product), add their GitLab handle to the "owner" column of the [projects CSV](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv).

</details>
