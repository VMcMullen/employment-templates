<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to the [Sales Quick Start Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop). New sales team member will receive an invitation to a public slack channel exclusively for your onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop.

</details>

<details>
<summary>Manager</summary>


1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs if you haven’t already.
1. [ ] Manager: Add any additional items to this issue template that are specific to your geo, region, or area such as the salesforce and clari dashboards for your region or area. Recommended reading, like [The Phoenix Project](https://www.amazon.com/dp/B078Y98RG8/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1), our [DevOps Getting Started Guide](https://learn.gitlab.com/beginners-guide-devops/guide-to-devops?utm_source=marketo&utm_medium=email&autm_campaign=newsletter&utm_content=20220331developernewsletter&mkt_tok=MTk0LVZWQy0yMjEAAAGDf1JyeBxer0ZJoK8-xxNnqlu7UmZVrgHGwaYKEmhqvysBNFzhnIoxnG04IKc9GwcfL_wxKd39OqhLpveI3vu7L_FXqAo5ZJ9KhEJ3Vv1IIF9n9Vk), or strategy documents.
1. [ ] Manager: Include an item in your weekly 1:1 to check-in on the SAL’s progress with onboarding. Guidance for Enterprise Sales is that a new hire should prioritize shadowing, bookmarking key resources, and completing the milestones in their role-based onboarding according to the relevant milestone (30-60-90) days. (View New Team Member section below for detail.)
1. [ ] Manager: Make the onboarding buddy you assigned aware that they should offer **live** customers calls for the new hire to shadow and connect the new hire with peers to shadow as well. Learn more on the [Customer Ready Shadow Program handbook page](https://about.gitlab.com/handbook/sales/shadow-program/) or by taking the [20-minute introduction training](https://rise.articulate.com/share/Ueitx5dXtEgisViFhF6qFw1fir-gOU1F) (Password is shadow2023).
1. [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab. 


</details>

<details>
<summary>Onboarding buddy</summary>

1. [ ] Onboarding buddy: Invite the new hire to shadow your live customer calls as part of the [Customer Ready Shadow Program](https://about.gitlab.com/handbook/sales/shadow-program/). Connect them with your peers to do the same.

1. [ ] Onboarding buddy: Take the [20-minute intro training to the shadow program](https://rise.articulate.com/share/Ueitx5dXtEgisViFhF6qFw1fir-gOU1F) (Password is shadow2023) so you know how the program works and can help out your new hire. 
</details>


<details>
<summary>New Team Member</summary>


**Guidance for role-based onboarding:** Your onboarding is meant to help you learn what you need to in order to do your everyday work as quickly as possible. When you think about prioritizing everything that's here: 

- Focus on bookmarking resources you'll need later, and giving them a quick review so you know when to refer back to them. 
- Begin shadowing your peers on customer calls to accelerate your learning. Your onboarding buddy can start offering you calls to shadow as soon as you complete the [20-minute intro training to the shadow program](https://rise.articulate.com/share/Ueitx5dXtEgisViFhF6qFw1fir-gOU1F) (Password is shadow2023).  
- And plan to complete the trainings in your role-based onboarding based on the milestones they align to (30-60-90 days) so you don't get overwhelmed. For example, you'll see that the main deliverable in your first 30 days is to complete your territory plan and share it with your manager for feedback.

**Key tasks**
- [ ] **Bookmark and review** the [Enterprise Sales handbook page](https://about.gitlab.com/handbook/sales/playbook/) and store it in a bookmarks folder in your toolbar for easy access. You’ll come back to it often.
- [ ] **Join the Field Slack channels:** View and join [these Slack channels](https://about.gitlab.com/handbook/sales/sales-google-groups/#field-slack-channels) based on your role and team. 
- [ ] Ask your manager to send you the link to any team-specific dashboards in Salesforce and Clari.
- [ ] **Bookmark the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing)** in Google Drive as well as the [Enterprise Sales](https://drive.google.com/drive/folders/14obWsdpYmaASD186cXyUIA19mk2Id_db) folder. You'll use these often.
- [ ] **Tools and access:** If you have any issues accessing something you need to do your job, go to the tools list in [this section of the Enterprise Sales Handbook page](https://about.gitlab.com/handbook/sales/playbook/#quick-links-to-common-tools) and click 'access request'. Or you can go to this [access request page](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#how-do-i-choose-which-template-to-use) here for help. 
    - [ ] Follow [these instructions](https://docs.oracle.com/en/cloud/saas/datafox/dfudf/set-up-and-administer-salesforce-integration.html#sfdcAdminsIntgInstConnectDFSF) to link your DataFox Account with your Salesforce Account.
    - Ask your manager if you need access to Zendesk Light Agent. 
    - If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access. 
    - [ ] Note: PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal automatically. PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters instead of requesting it.
- [ ] **Login to Level Up (Thought Industries)** and begin working through your role-based onboarding: [Enterprise Sales: What every SAL needs to know (30-60-90 days)](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/enterprise-sales-what-every-sal-needs-to-know-30-60-90-days). 
     - Access is granted about 1 week after you start. 
     - Begin with the **Sales Quick Start** section followed by the **What to know in the first 30 days section.** Sales Quickstart will help you learn things like our sales methodology, the GitLab Direction, our competitive landscape, our product, and tools like the account planning tool in Gainsight. 
- [ ] Order and expense your [business cards](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/frequently_requested/#ordering-business-cards) from Moo.
</details>


