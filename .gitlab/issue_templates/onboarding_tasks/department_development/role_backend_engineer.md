#### Backend Engineering

<details>
<summary>New Team Member</summary>

1. [ ] Join the [`#database` channel](https://gitlab.slack.com/archives/C3NBYFJ6N) on Slack.
1. [ ] Familiarize yourself with the [Database Lab Engine](https://docs.gitlab.com/ee/development/database/database_lab.html#generate-query-plans). This can be helpful to verify query plans and execute DDL statements in a clone of the production database.
1. [ ] [Register](https://sentry.gitlab.net/gitlab/) and [familiarize](https://about.gitlab.com/handbook/support/workflows/sentry.html) yourself with Sentry, our internal errors monitoring tool.

##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.
1. [ ] Review [the MR acceptance checklist](https://docs.gitlab.com/ee/development/code_review.html#acceptance-checklist) which is used to help prevent production outages

</details>
