### For team members located in Singapore

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Fill out the [Singapore Benefits Onboarding Form](https://docs.google.com/forms/d/e/1FAIpQLSfma2tC9yu7fNP6yshpKsxiI47vXGhgUodGQfptSK608nBOZw/viewform?usp=sf_link) to enroll in the [Singapore Benefits Plans](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/singapore-pte-ltd/#medical)  


</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Download the following documents from [Egnyte](https://globalupside.egnyte.com/app/index.do#storage/files/1/Shared/Clients/GU/Gitlab/Onboarding%20Documents) (Global Upside's online portal):
    - Employee Personal Particulars Form
    - Bank Details Form
    - Consent form 
    - Id/social security cert (front & back)
    - Bank letter
1. [ ] People Connect: Check all required payroll documents have been sent, completed and uploaded to Workday.
1. [ ] People Connect: In the team member's onboarding issue, ping the Non-US Payroll team member `@nprecilla` and `@sszepietowska` letting them know that the Payroll questionnaire is completed and in the Payroll Forms folder. 
1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
