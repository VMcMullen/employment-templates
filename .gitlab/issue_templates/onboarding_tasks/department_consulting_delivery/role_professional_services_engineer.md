#### Professional Services Engineer

<details>
<summary>New Team Member</summary>

**Certifications**
1. [ ] Complete all [GitLab Certifications](https://about.gitlab.com/services/education/gitlab-technical-certification-self-paced/) on GitLab Learn.
    - [ ] [GitLab Associate](https://levelup.gitlab.com/learn/course/gitlab-ci-cd-specialist/)
    - [ ] [GitLab CI/CD Specialist](https://levelup.gitlab.com/learn/course/gitlab-ci-cd-specialist-self-paced-bundle/)
    - [ ] [GitLab Security Specialist](https://levelup.gitlab.com/learn/course/gitlab-certified-security-specialist/)
    - [ ] [GitLab Project Manager Specialist](https://levelup.gitlab.com/learn/course/gitlab-certified-project-management-specialist-bundle/)

**Cross-team Collaboration**
1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls. Refer to the [support shadow process](https://about.gitlab.com/handbook/support/#support-shadow-program) to help you schedule these call.
2. [ ] Work with your Manager to Update our [Skills Matrix](https://docs.google.com/spreadsheets/d/1SaojIbH875KuPmls_oChiRACF2HOFi0I-p_vJ1B9t40/edit#gid=0) so that Operations find Engagements you will be successful with in the future.

**GET Workshop/Implementation Certification**
1. [ ] Work through the [Implementation Engineer Specialist](https://levelup.gitlab.com/learn/course/gitlab-certified-implementation-specialist-bundle/). In this certificationk you will complete the [GET Deployment Workshop](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/get-deployment-workshop). :exclamation: The instance created here will be used in the next sections. DO NOT DELETE.

**Advanced Technical Topics**
1. [ ] Backup omnibus using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
1. [ ] Install GitLab via [Docker](https://docs.gitlab.com/ce/install/docker.html)
1. [ ] Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)
1. [ ] Starting a [Rails Console](https://docs.gitlab.com/ee/administration/operations/rails_console.html)
1. [ ] Learn about the [GitLab Rake](https://docs.gitlab.com/ee/raketasks/) command
1. [ ] Learn about [GitLab Omnibus Maintenance](https://docs.gitlab.com/omnibus/maintenance/) commands
1. [ ] Browse/get familiar with the [GitLab Support Templates](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates)
1. [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)

**Custom Integrations**
**Goal:** Understand how developers and customers can produce custom integrations to GitLab. If you want to integrate with GitLab there are three possible paths you can take:
1. [ ] [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
1. [ ] [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
1. [ ] [Project Integrations](https://docs.gitlab.com/ee/user/project/integrations/) -  Integrations are like plugins, and give you the freedom to add functionality to GitLab.

**Migrations**
**Goal:** Understand how migrations from other Version Control Systems to GitLab using [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) work.
1. [ ] Work through the [Migration Engineer Specialist Certification](https://levelup.gitlab.com/learn/course/gitlab-certified-migration-specialist-bundle/). In this course, you will complete the[Congregate Migration Workshop](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate-onboarding-workshop)

</details>

