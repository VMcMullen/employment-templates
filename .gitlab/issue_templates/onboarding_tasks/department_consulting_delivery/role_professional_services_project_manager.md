#### Professional Services Project Manager

<details>
<summary>New Team Member</summary>

_Welcome to Project Manager Onboarding! This issue will help you get ramped up on being able to deliver and manage professional services projects._

:warning: If you find something that can be improved, please submit a merge request to this [issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/onboarding_tasks/department_practice_management/role_engagement_manager.md) and send to the [#engagement-managers](https://gitlab.slack.com/archives/C021J8Z88AJ) slack channel for review. 

## Section 1: Understanding our business, services and processes

_Weeks 2-4_

- [ ] Make sure you are enrolled in [Sales Quick Start (SQS)](https://about.gitlab.com/handbook/sales/onboarding/). If you are not, reach out to your manager to ensure you're scheduled. 
- [ ] Watch the latest high level professional services enablement [recording](https://gitlab.edcast.com/insights/sqs-22-professional). And/or check out the [associated slides](https://docs.google.com/presentation/d/1SIg3xYyFVQIPEe36VpsCJqt3sDp5boma4YpICRQMrEA/edit#slide=id.g75e3e400c6_6_123).
- [ ] Review the high level [professional services methodology](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/processes/) to understand how the Project Manager workflows fit into the larger Sales process. 
    - [ ] Learn Project Management workflow [Project Management Handbook](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/project-mgmt/)
    - [ ] Learn Agile Project Management with GitLab [Agile Project Management](https://www.youtube.com/watch?v=_dndOgHLvsM)
    - [ ] Understand how to navigate [SalesForceDotCom (SFDC) Fields & Reports](https://www.loom.com/share/111a577fbd8a4d6baeea1f2aee86dff2)
    - [ ] Review the different types of [professional services offerings and delivery kits available](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
        - [ ] Learn about key details that tend to come up in [Migration Opportunities (25 mins)](https://www.loom.com/share/e02695d1092f476aaabdbd48e4c0a3f4?sharedAppSource=personal_library)
        - [ ] Understand the transformational services that we call the [Pipeline COE (8 mins)](https://www.loom.com/share/1c8e74a0728245a5b49274ba668e750e?sharedAppSource=personal_library)
        - [ ] Learn the fundamentals of [Distributied Systems Architecture (18 mins)](https://www.loom.com/share/6c6acebabce2474589eab0bea1879c2b?sharedAppSource=personal_library) to ensure you can speak the language required to sell/scope implementation services. 
            - [ ] Understand the basics of [Disaster Recovery (12 mins)](https://www.loom.com/share/535dddbe3eb247938fb2ba2e98eb625b?sharedAppSource=personal_library)
            - [ ] Learn about the [deployment Automation & Cloud Maturity (8 mins)](https://www.loom.com/share/9bd2745dbabb4384ac9a8aa369d79448?sharedAppSource=personal_library) 
        - [ ] Understand the differences between [GitLab SaaS vs Self Managed (7 mins)](https://www.loom.com/share/4772f2acf51d4d2a874ca2ce583fd94a?sharedAppSource=personal_library)
    - [ ] Review the different types of [education services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/#current-offerings) and when to position them.  
    - [ ] Learn how we encourage the Sales Account teams to [sell professional services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/). Make sure to take note of the custom SOW process vs the standard SKU process. 
- [ ] Review the README files in the [Professional Services Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) to understand in more detail how we deliver services. 
    - [ ] Its important to note the `customer` foloder in the [Migration Template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template) Project. There is lots of information about setting customer's expectations
    - [ ] The [migration customer documents](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/-/tree/master/customer) are a great place to learn more about migration features and checklists that are discussed with the customer during pre-sales.
    - [ ] [Implementations](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-template)
    - [ ] [Readiness Assessments](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/readiness-assessment) (Health Checks)
    - [ ] Rapid Results ([SaaS](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-com)) ([self-managed](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-self-managed))
- [ ] Spend some the time in the [Practice Management folder](https://drive.google.com/drive/folders/1r0vqvcZObuLWYfa41mYHR3XJCmCKtiiO).  

## Section 2 - Schedule Coffee Chats

_Weeks 2-4_

- [ ] Schedule/Attend Coffee Chats:
_See the [roster](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#team-members-and-roles) to find teammates names._
    - [ ] Have a coffee chat with peer Engagement Managers
    - [ ] Have a coffee chat with Practice Manager, Consulting Services
    - [ ] Have a coffee chat with Practice Manager, Education Services
    - [ ] Have a coffee chat with Project Coordinator to discuss back office operations and pre-sales to post-sales handoff
    - [ ] have a coffee chat with the Consulting Delivery Leadership team to discuss recent customer engagements

## Section 3 - Access

_Weeks 2-4_

- [ ] Make sure you've been added to the following slack channels/groups. If you haven't make sure to fill out an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to be added.
    - [ ] [#professional-services](https://gitlab.slack.com/archives/CFRLYG77X) 
        - General public channel for the company to communicate with PS.  Here PS answers questions, and collaborates with the account teams on PS opportunities.
    - [ ] [#ps-project-leadership](https://gitlab.slack.com/archives/GR4A7UJSF)
        - Private channel  used for discussing project coordination and project management with PS leadership.
    - [ ] [#ps-operations](https://gitlab.slack.com/archives/CNP5X078T)
        - Private channel is used to have conversations around Professional Services Operations, any billing, revenue and backend functions.
    - [ ] [#congregate-internal](https://gitlab.slack.com/archives/GRYB20ZA5)
        - Private channel for collaborating on PS migrations using Congregate.
    - [ ] [#ps_proservp](https://gitlab.slack.com/archives/GFY1QN4FJ)
        - Private channel used for internal PS team communications.
    - [ ] PM's slack group handle '@ps-pmo' 
- [ ] Review and Validate system accesses:
    - [ ] MavenLink
- [ ] If you've found that you're missing one or more access, please submit an MR to update the [engagement manager role based access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_consulting_delivery/role-engagement-manager.md)

## Section 4 - Review Customer Calls + Shadowing

_weeks 3-6_
- [ ] Review the below recordings, documents and templates that will help understand the different types of conversations the PM has with customers. 
- [ ] Login to https://chorus.ai/.  This is a video recording application, and all Customer KickOff and review calls are stored there.  
- [ ] Shadow a peer Project manager on 5 opportunities from initial touchpoint with the account team all the way through to Project Close. Make a note of each in a comment on this issue. In each comment, make a note of the notes doc, scoping issue, and meetings scheduled to talk with that customer. 
    - [ ] Opportunity 1
    - [ ] Opportunity 2 
    - [ ] Opportunity 3 
    - [ ] Opportunity 4
    - [ ] Opportunity 5

| PM Step/Task | Customer Recording | Collateral | Template | 
| ------ | ------ | ------ | ------ |
| Large Customer Kickoff | Recording TBD | Notes TBD | Template TBD |
| Large Customer A Discovery | Template TBD |


## Section 5 - Find an opportunity to lead an opportunity

_Week 6-12_

- [ ] Lead your first customer delivery. 
- [ ] Use feedback from previous PM to improve on offering and gain approval from previous PM to iterate and improve. 

</details>
