#### Professional Services Engagement Manager

<details>
<summary>New Team Member</summary>

_Welcome to Engagement Manager Onboarding! This issue will help you get ramped up on being able to scope, position and sell services._

:warning: If you find something that can be improved, please submit a merge request to this [issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/onboarding_tasks/department_practice_management/role_engagement_manager.md) and send to the [#engagement-managers](https://gitlab.slack.com/archives/C021J8Z88AJ) slack channel for review. 

## Section 1: Understanding our business, services and processes

_Weeks 2-4_

- [ ] Make sure you are enrolled in [Sales Quick Start (SQS)](https://about.gitlab.com/handbook/sales/onboarding/). If you are not, reach out to your manager to ensure you're scheduled. 
- [ ] Watch the latest high level professional services enablement [recording](https://gitlab.edcast.com/insights/sqs-22-professional). And/or check out the [associated slides](https://docs.google.com/presentation/d/1SIg3xYyFVQIPEe36VpsCJqt3sDp5boma4YpICRQMrEA/edit#slide=id.g75e3e400c6_6_123).
- [ ] Review the high level [professional services methodology](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/processes/) to understand how the Engagement Manager workflows fit into the larger Sales process. 
- [ ] Enroll in the [EM Certification Learning Pathway](https://levelup.gitlab.com/learn/course/engagement-manager-accreditation). Note: we are moving to a new learning management platform in August 2022. If this link doesn't work, reach out to your manager to get the new link.  
- [ ] Add a shortcut to the [Engagement Management folder](https://drive.google.com/drive/folders/1tEl6quPPujqGnz94isLFh8x9_DfKPpdG?usp=sharing) to your Google drive.
<!---    - [ ] Learn how the account teams request scoping support using the [Services Calculator](https://www.loom.com/share/4f73db559b31418ebff7524681dc106a)
    - [ ] Learn how to triage the [Scoping Issue](https://www.loom.com/share/f1b0810484bf48ef813d9dfdbe9ef43a) that is generated from the services calculator. 
    - [ ] Understand the contract commercial terms we use - [T&M v Fixed Price](https://www.loom.com/share/e219c00b23b943039005dc100b70b536)
    - [ ] Understand how to navigate [SalesForceDotCom (SFDC) Fields & Reports](https://www.loom.com/share/111a577fbd8a4d6baeea1f2aee86dff2)
    - [ ] Review the different types of [professional services offerings and delivery kits available](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
        - [ ] Learn about key details that tend to come up in [Migration Opportunities (25 mins)](https://www.loom.com/share/e02695d1092f476aaabdbd48e4c0a3f4?sharedAppSource=personal_library)
        - [ ] Understand the transformational services that we call the [Pipeline COE (8 mins)](https://www.loom.com/share/1c8e74a0728245a5b49274ba668e750e?sharedAppSource=personal_library)
        - [ ] Learn the fundamentals of [Distributied Systems Architecture (18 mins)](https://www.loom.com/share/6c6acebabce2474589eab0bea1879c2b?sharedAppSource=personal_library) to ensure you can speak the language required to sell/scope implementation services. 
            - [ ] Understand the basics of [Disaster Recovery (12 mins)](https://www.loom.com/share/535dddbe3eb247938fb2ba2e98eb625b?sharedAppSource=personal_library)
            - [ ] Learn about the [deployment Automation & Cloud Maturity (8 mins)](https://www.loom.com/share/9bd2745dbabb4384ac9a8aa369d79448?sharedAppSource=personal_library) 
        - [ ] Understand the differences between [GitLab SaaS vs Self Managed (7 mins)](https://www.loom.com/share/4772f2acf51d4d2a874ca2ce583fd94a?sharedAppSource=personal_library)
    - [ ] Review the different types of [education services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/#current-offerings) and when to position them.  
    - [ ] Learn how we encourage the Sales Account teams to [sell professional services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/). Make sure to take note of the custom SOW process vs the standard SKU process. 
    - [ ] Review the [Professional Services Engagement Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/) handbook page, including scoping and processes.
- [ ] Review the README files in the [Professional Services Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) to understand in more detail how we deliver services. 
    - [ ] Its important to note the `customer` foloder in the [Migration Template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template) Project. There is lots of information about setting customer's expectations
    - [ ] The [migration customer documents](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/-/tree/master/customer) are a great place to learn more about migration features and checklists that are discussed with the customer during pre-sales.
    - [ ] [Implementations](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-template)
    - [ ] [Readiness Assessments](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/readiness-assessment) (Health Checks)
    - [ ] Rapid Results ([SaaS](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-com)) ([self-managed](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-self-managed))
- [ ] Spend some the time in the [Engagement Management folder](https://drive.google.com/drive/folders/1tEl6quPPujqGnz94isLFh8x9_DfKPpdG).  It includes many sub-folders where you can spend time reviewing (Cost of Goods Sold)COGs, SOW templates, Scoping documents, and presentations.  SOW examples are available [here](https://drive.google.com/drive/folders/1J1HqK6lh36UYLnyRcpe3u-Hpl5xSy0Vp) for review as well. 
--->

## Section 2 - Schedule Coffee Chats

_Weeks 2-4_

- [ ] Schedule/Attend Coffee Chats:
_See the [roster](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#team-members-and-roles) to find teammates names._
    - [ ] Have a coffee chat with peer Engagement Managers
    - [ ] Have a coffee chat with Practice Manager, Consulting Services
    - [ ] Have a coffee chat with Practice Manager, Education Services
    - [ ] Have a coffee chat with Project Coordinator to discuss back office operations and pre-sales to post-sales handoff
    - [ ] have a coffee chat with the Consulting Delivery Leadership team to discuss recent customer engagements

## Section 3 - Access

_Weeks 2-4_

- [ ] Make sure you've been added to the following slack channels. If you haven't make sure to fill out an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to be added.
    - [ ] [#professional-services](https://gitlab.slack.com/archives/CFRLYG77X) 
        - General public channel for the company to communicate with PS.  Here PS answers questions, and collaborates with the account teams on PS opportunities.
    - [ ] [#ps-project-leadership](https://gitlab.slack.com/archives/GR4A7UJSF)
        - Private channel  used for discussing project coordination and project management with PS leadership.
    - [ ] [#ps-operations](https://gitlab.slack.com/archives/CNP5X078T)
        - Private channel is used to have conversations around Professional Services Operations, any billing, revenue and backend functions.
    - [ ] [#ps-engagement-mgrs](https://gitlab.slack.com/archives/C021J8Z88AJ)
        - Private channel used for PS engagement management collaboration and coordination.
    - [ ] [#congregate-internal](https://gitlab.slack.com/archives/GRYB20ZA5)
        - Private channel for collaborating on PS migrations using Congregate.
    - [ ] [#proliferate-internal](https://gitlab.slack.com/archives/G013HAR4EJZ)
        - Private channel used for collaborating on PS implementations using Proliferate.
    - [ ] [#ps_proservp](https://gitlab.slack.com/archives/GFY1QN4FJ)
        - Private channel used for internal PS team communications.
    - [ ] EM's slack group handle '@em' - https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/9346 
- [ ] Make sure you've been added to the `@em` group in Slack
- [ ] Review and Validate system accesses:
    - [ ] Salesforce.com
    - [ ] MavenLink
    - [ ] DocuSign
- [ ] If you've found that you're missing one or more access, please submit an MR to update the [engagement manager role based access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_practice_management/role-engagement-manager.md)

## Section 4a - Review Customer Calls + Shadowing

_weeks 3-6_
- [ ] Review the recordings in the table below, documents and templates that will help understand the different types of conversations the EM has with customers. 
- [ ] Login to https://chorus.ai/.  This is a video recording application, and all scoping and estimate or SOW review calls are stored there.  
    - [ ] Once you have access search for "Julie Byrne" and you will find all calls that Julie attended/led.  We recommend watching one per day 
- [ ] Shadow a peer engagement manager on 5 opportunities from initial touchpoint with the account team all the way through to closed won/lost. Make a note of each in a comment on this issue. In each comment, make a note of the notes doc, scoping issue, and meetings scheduled to talk with that customer. 
    - [ ] Opportunity 1
    - [ ] Opportunity 2 
    - [ ] Opportunity 3 
    - [ ] Opportunity 4
    - [ ] Opportunity 5

| EM Step/Task | Customer Recording | Collateral | Template | 
| ------ | ------ | ------ | ------ |
| Large Customer Introductory | Recording TBD | Notes TBD | Template TBD |
| Large Customer A Discovery | [Large Customer A Estimate Discovery Call](https://chorus.ai/meeting/8B3988FFB49B4FBCBA01B4EB9D6D3465?tab=summary)  | [Large Customer A Discovery Notes Doc](https://docs.google.com/document/d/1RC5nU2oxo3UYiueW6G2nkhPb9aTxHayrQW1kHeVAn54/edit#) | Template TBD |
| Internal estimate building (Large Customer A) | [Estimate Recording]() | [Estimate Worksheet](https://docs.google.com/spreadsheets/d/14wyCtF4aPCo9nEh2hZ7olx_mRq4ziLmysMRoKcwtIdQ/edit#gid=498273375) | [Estimate Template](https://docs.google.com/spreadsheets/d/1YKMyflzsA-VPEVobB82zC8-n0hlC-uRBtiNB7Fm-kZg/edit#gid=498273375)
| Large Customer B Discovery | [Large Customer B Discovery Call](https://chorus.ai/meeting/781ADAC9197844F0A571FD05382366B9?ro_company_id=9765688)  |

## Section 4b - Review Selling Automation and SKU Process (Commercial EM ONLY)
- [ ] Understand how Account teams can quote PS SKUs directly on a license quote or on a separate PS Only Opportunity in SFDC. 
- [ ] Learn about the SKU creation process ([handbook](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/practice-mgmt/#how-to-create-a-new-service-sku), [example SKU](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/4482))
- [ ] Schedule a chat with Bryan on this to ask questions as the SKU process. He can discuss the revenue, accounting and quoting tool constraints involved in the SKU creation process.  








## Section 5 - Product and Sales Training

_Weeks 4-8_

- [ ] Complete the ([GitLab with Git Basics course in edCast.](https://levelup.gitlab.com/courses/gitlab-certified-git-associate-prep)). Make sure that you use the employee discount code in the link to enroll for free.
- [ ] Compelte the [GitLab for Project Managers Course](https://levelup.gitlab.com/courses/gitlab-certified-project-management-specialist-bundle). Make sure that you use the employee discount code in the link to enroll for free.
- [ ] Review [how to use GitLab planning features](https://about.gitlab.com/handbook/marketing/strategic-marketing/getting-started/101/) to collaborate and manage work

## Section 6 - Review Opportunity Pipeline
_Weeks 4-8_

- [ ] Login to Salesforce.com and review Pipeline:
    - [ ] Begin to review Regional Opportunities in Salesforce.com. This ['Open Opps' report](https://gitlab.my.salesforce.com/00O4M000004aGyC) provides an overview of All Open Professional Services Opportunities. Filter to show 'Current FQ' to start.
    - [ ] Begin to 'Follow' Opportunities where appplicable or of interest
    - [ ] As you begin to 'lead' Opportunities, review [this handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/tracking-opps/), and begin to update Salesforce.com accordingly.   
- [ ] Begin scheduling shadowing opportunities to prepare for the next section

## Section 7 - Final Assessment

_Weeks 5-10_

- [ ] Review the final [EM Onboarding assignment](https://docs.google.com/document/d/1FdQpQOe_OkjvA3svAsjGfsaiDoUAFs_h0LPVYzIkvdI/edit) and associated [grading rubric](https://docs.google.com/spreadsheets/d/1HLzVSgoSJ6zDltusq8KnkpHoyDxByumko7N90Rr5MQk/edit). You will be expected to create a propsal and SOW to a panel of your peers who will act as the mock customer and/or account team. This will validate your understanding of our services, processes and overall ability to articulate our value proposition. This exercise is supposed to simulate a real customer scoping opportunity, so the process of delivering this will happen over a number of synchronous and asynchronous meetings:
    - [ ] scoping issue created - gather info asynchronously
    - [ ] Check out the [grading rubric](https://docs.google.com/spreadsheets/d/1HLzVSgoSJ6zDltusq8KnkpHoyDxByumko7N90Rr5MQk/edit?usp=sharing) we will use to evaluate your proposal.
    - [ ] initial proposal call based on current understanding
    - [ ] revised proposal call based on updated understanding (note this could happen multiple times until we can agreement in principal on the services proposal)
    - [ ] SOW review - asynch unless meeting needed


## Section 8 - Find an opportunity to lead an opportunity

_Week 6-12_

- [ ] Lead the scoping of an engagement with an account team with peer engagement manager is observing and providing feedback. 
- [ ] Use feedback from previous EM to improve on offering and gain approval from previous EM to scope deals solo. 

</details>
