#### Product Designers

<details>
<summary>New Team Member</summary>

1. [ ] Join the [#ux](https://gitlab.slack.com/messages/ux/) channel on Slack.
1. [ ] Join the [#ux_research](https://gitlab.slack.com/archives/CMEERUCE4) channel on Slack.
1. [ ] Familiarize yourself with the [UX Designer Onboarding](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/) page and [relevant pages](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/#relevant-links) linked from there.
1. [ ] Your specific UX team buddy is: (FILL IN WITH @ HANDLE).
1. [ ] Edit your [team page](https://about.gitlab.com/company/team/) entry to add yourself to the [list of UX reviewers of the GitLab project](https://about.gitlab.com/handbook/engineering/projects/#gitlab_reviewers_UX) (all Product Designers are by default). Add the following to your file, replacing any existing departments and adding the following projects:
   ```yml
   departments:
     - Engineering Function
     - UX Department
   projects:
     gitlab: reviewer UX
     gitlab-design: maintainer 
   ```
</details>

<details>
<summary>Manager</summary>

1. [ ] An access request will be automatically created for your new team member on their second day using the [Product Designer Access Request template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_ux/role_product_designer.md). This includes:
    1. `@uxers` slack handle
    1. `ux-department@gitlab.com` email
    1. `@gitlab\\-com/gitlab\\-ux` group
    1. dev.gitlab.com
1. [ ] Give new team member `Developer` access to the [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives) project on GitLab.com.
1. [ ] Give new team member `Developer` access to the [GitLab UX](https://gitlab.com/gitlab-com/gitlab-ux) group on GitLab.com.
1. [ ] Add new team member to the designer and stage group [mention groups](https://gitlab.com/gitlab-com/gitlab-ux) for mentioning in GitLab.
1. [ ] Share the UX calendar with new team member (Access level: _Make changes to events_).
1. [ ] Add new team member to UX weekly calls.
1. [ ] Add new team member to UX Showcase calls.
1. [ ] Add new team member to relevant [product categories](https://about.gitlab.com/handbook/product/categories/).
1. [ ] Add new team member to the Reviewer Roulette by updating their .yml file with Project - Reviewer UX. [Example](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team_members/person/e/emilysybrant.yml). Remind them they can [avoid being picked](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) before they are ready to review an MR.

</details>

<details>
<summary>UX Foundation Manager</summary>

1. [ ] UX Foundations: Add new team member to the [Figma Group](https://www.figma.com/files/team/749263947527384695/GitLab/members) with **edit** access. - @tauriedavis

</details>
<details>
<summary>UX Buddy</summary>

1. [ ] UX buddy: Create onboarding issue in the [GitLab Design](https://gitlab.com/gitlab-org/gitlab-design/issues/new) project on GitLab.com.

</details>
