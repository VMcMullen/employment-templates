### Before Starting at GitLab - Team Members in Japan

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Two weeks before the team member's start date, send the forms below for them to complete, with the hire date as the effective date, be sure to copy the Non-US Payroll team. 
  * [ ] [Employee Registration Form_BDO](https://docs.google.com/spreadsheets/d/1IBkW5c7D4e5zItJ4VswBNaFixtZgnIUA/edit#gid=1134651071)
        - select make a copy and save in team members name. Share with team members personal email address. 
  * [ ] [Application for (change in) exemption for dependents of employment income earner](https://drive.google.com/drive/u/0/folders/1zQ0aYTeor59dszMfYH4j7D4a3OB8Z9_Q)
        - Download and email the form to the team members personal email address to complete.
1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Once the completed documents have been received upload them to Workday.

</details>
