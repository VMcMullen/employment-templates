### Day 1 - For New Team Members in the UK 

<details>
<summary>People Connect</summary>

**Prior to Start Date**
1. [ ] People Connect: Confirm that you have submitted the onboarding form in Papaya Global's online portal for the new team member - this needs to be done at least one week before the start date.

**Post Start Date**
1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Download the new team member's payroll documents i.e. the P45 and New Starter Checklist as applicable from the Papaya Global [portal](https://gitlab.papayaglobal.com/backoffice/org/1826/dashboard) i.e. click on GitLab UK, search for the team members name and upload them to Workday following the guidelines in the [job aid](https://docs.google.com/document/d/1ao_d_JxvqvZdqxlt4mBoHe1GcAhYT7B6YQoBgDxPdRE/edit). 
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>



<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Confirm with your assigned People Connect Team member that you have uploaded your P45 or New Starter Checklist to the Papaya Global portal.
1. [ ] New Team Member: Populate your Banking Details within your Workday Profile by following the process outlined in the Payment Elections [Job Aid](https://drive.google.com/file/d/19vH0uKMlDQDF8EaGEg2dW6sNIcnjJSrM/view?usp=sharing).
1. [ ] New Team Member: Read through the AXA PPP Brochure posted in the 'Medical Insurance' section of the [UK Benefits Handbook Page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#medical-insurance) and let the Total Rewards team know if you and your dependents where applicable would like to join the medical insurance scheme by emailing them at `total-rewards@gitlab.com`. 
1. [ ] New Team Member: Please confirm that you have seen and read the `Certificate of Employers' Liability Insurance` as linked in the [Employers' Liability Insurance](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#employers-liability-insurance) section.
1. [ ] New Team Member: Please read through the auto-enrollment personal pension details which you can find on the [pensions section](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#pension-introduction) of the benefits page. If you wish to opt in to Salary Sacrifice for the pension Scheme, please email nonuspayroll@gitlab.com with the percentage of your salary that you would like to sacrifice.
1. [ ] New Team Member: Please read the [Automatic Enrollment Letter](https://drive.google.com/file/d/1CMrj8BVJ0Omk0A1CHVXnhkI9QjjOoVxt/view?usp=sharing) for the GitLab Pension Plan. If you do not believe you qualify for automatic enrollment, please notify the Total Rewards Team, total-rewards@gitlab.com

</details>

<details>
<summary>Legal</summary>

1. [ ] @sarahrogers: Confirm that the team member has reviewed and acknowledged home working risk assessment and guidelines for working comfortably from home within their Workday Onboarding Tasks.

</details>
