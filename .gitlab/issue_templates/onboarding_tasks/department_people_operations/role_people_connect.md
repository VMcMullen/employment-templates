## For People Connect Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to the following private Slack channels (as applicable):
   - `#people-group-confidential`
   - `#people-exp_ces`
   - `#connect-ops-team`
   - `#peopleops_totalrewards`
   - `#payroll-peopleops`
   - `#pbp-peopleops`
   - `#international-exp`
   - `#employment-surveys`
   - `#offboardings`
   - `#people-ops-tech`
1. [ ] Manager: Add team member to the following public Slack channels (as applicable):
   - `#peopleops-alerts`
   - `#people-group`
   - `#people-connect`
1. [ ] Manager: Add team member to the following Slack groups (as applicable):
   - `people-connect`

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: @ashjameson to add team member to Greenhouse as "Job Admin: People Success" following these [steps](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions).
1. [ ] People Connect: If the new team member is on the Compensation Team or an [Executive](https://about.gitlab.com/company/team/structure/#executives), please notify @ashjameson using the [Greenhouse Approvals Change Request](https://gitlab.com/gl-recruiting/operations/issues/new#) Issue template.
1. [ ] People Connect: Share the People Connect Shared [Drive](https://drive.google.com/drive/folders/0ABRi3YnroHzzUk9PVA) with the team member.
1. [ ] People Connect: Invite the team member to the [People Group Calendar](https://calendar.google.com/calendar/embed?src=c_i9nueu50c6ddsq7s6odraihhi0%40group.calendar.google.com&ctz=America%2FNew_York) in Google Calendar.

</details>

<details>
<summary>New Team Member</summary>

1. You will be added to the email alias for your team. This can result in a high volume inbox, consider reviewing how your fellow team members are responding to the queries we as a team receive. No need to respond to any of these queries during week 1 or 2.
1. [ ] New Team Member: Review [key Slack channels](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/Key_Slack_Channels.md) for the People Connect team to familiarze yourself with important channels and their purpose.

</details>
