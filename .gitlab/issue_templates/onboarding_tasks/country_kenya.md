### For Team Members in Kenya

<details>
<summary>New Team Member</summary>

1. [ ] Take a look at the Kenya benefit [page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india/#kenya).

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>

