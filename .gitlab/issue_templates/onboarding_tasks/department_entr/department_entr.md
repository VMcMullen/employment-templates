#### Sales Division

<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to the [Sales Quickstart Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop) cohort. New sales team member will receive an invitation to a public slack channel exclusively for their onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop.
</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Create an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) and selecting "Territory_Change_Request" under the **Description** drop-down to populate the template. Once submitted it will inform Sales Ops of the territory that the new team member will assume. Assign the issue to your sales operations partner. If unsure about who your sales operations partner is, ask your manger.

</details>







