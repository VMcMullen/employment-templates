## Development Department

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Ensure that the `job title specialty` indicated in Workday accurately reflects the team member's new group/team.
1. [ ] If this team member will be the DRI for one or more [tracked repositories](https://about.gitlab.com/handbook/engineering/metrics/#projects-that-are-part-of-the-product), add their GitLab handle to the "owner" column of the [projects CSV](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv).

</details>
