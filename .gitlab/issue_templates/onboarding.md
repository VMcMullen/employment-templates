 ## Welcome to your GitLab Onboarding Issue! :tada:


![swag](https://gitlab.com/gitlab-com/people-group/peopleops-eng/emails/-/raw/f300f5097b8364158541bc447ad71af2d6fad249/images/swag.png)

We look forward to setting you up for [all-remote](https://about.gitlab.com/company/culture/all-remote/guide/) success as your team member journey begins.

### Table of Contents and Areas of Focus

| Timing | Description |
| ------ | ------ |
| [**Before You Start**](#accounts-and-access) | Accounts and Access |
| **[Day 01](#day-1-getting-started-accounts-and-paperwork)** | Getting Started: Accounts and Onboarding|
| **[Day 02](#day-2-remote-working-and-our-values)** | Remote Working and our Values|
| **[Day 03](#day-3-security-compliance)** | Security & Compliance |
| **[Day 04](#day-4-social-benefits)** | Social & Benefits |
| **[Day 05](#day-5-git)** | Introduction to Git and using GitLab |
| **[Day 06](#weeks-2-4-explore)** | Explore (Various) | 
| **[Week 2+](#job-specific-tasks)** | Job Specific Tasks | 


You may be eager to jump right into your role but we encourage you to set aside dedicated time, at least one to two weeks, to focus on these tasks as they are geared toward ensuring you are enabled to [thrive in an all-remote environment](https://about.gitlab.com/company/culture/all-remote/onboarding/#the-importance-of-onboarding). You do not, however, have to complete all the tasks in their presented order (except in the case of the :red_circle: compliance tasks). We encourage you to be a [manager of one](https://about.gitlab.com/handbook/values/#managers-of-one) and discuss any questions with your manager.

### Introduction

This is your core GitLab Onboarding Issue and it is housed in GitLab.com in the interests of what is known as [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding). It has been crafted with the [three dimensions of remote onboarding](https://about.gitlab.com/company/culture/all-remote/onboarding/) in mind i.e. organizational, technical and social onboarding.

All three of these are underpinned by the sentiments of [self-directed and continuous learning](https://about.gitlab.com/company/culture/all-remote/self-service/), using a diverse set of high and low touch resources available to all GitLab team members including videos, handbook pages, blogs, and knowledge assessments while encouraging [asynchronous communication and workflows](https://about.gitlab.com/company/culture/all-remote/asynchronous/).

### Support and using the GitLab Handbook

GitLab is a [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/) organization and has both a [publicly available handbook](https://about.gitlab.com/handbook/) and an [internal handbook](https://internal-handbook.gitlab.io/) which is only accessible by team members - both of these contain information that will answer many of the questions you may have in your first thirty days. 

**Important Note: [Search GitLab like a pro](https://about.gitlab.com/handbook/tools-and-tips/searching/)! Set up your browser to utilize [keyword searches](https://about.gitlab.com/handbook/tools-and-tips/searching/#setting-up-keyword-searches-on-chrome) to be able to search our handbook with ease.** 

If you are unable to find the information you are looking for after searching, please reach out to your Manager (`__MANAGER_HANDLE__`) or Onboarding Buddy (`__BUDDY_HANDLE__`). General People Team inquiries around onboarding, benefits and compensation can be made in Slack via the #people-connect channel. If you need to troubleshoot specific issues with applications, tools or your laptop reach out to IT Operations in the #it-help channel. 

There is a lot to absorb and discover in the next few days - as you work through the linked handbook pages be sure to bookmark those you'd like to refer back to or review in greater detail to once you have had time to settle.

Clicking on the links detailed within the body of this issue will move you away from this page, right click and open these in a new tab if you would like to keep your issue handy while you review the handbook.

### :red_circle:  Onboarding Compliance

Our goal is to protect your privacy as a new team member while also adhering to the standards we need to have as a company. We have incorporated tasks within your onboarding issue that relate to compliance and keeping GitLab secure.

These tasks are marked with a red circle :red_circle: and completion of them is **required** and subject to audit by various teams such as the Security Team so please be sure to acknowledge them once complete by checking the relevant box.


---

### Before Starting at GitLab

<details>
<summary>Manager Tasks</summary>

1. [ ] Check your calendar! If you are planning any PTO during the first week of onboarding please contact an alternative team member to assist during your time away. Please assign your alternative team member to this issue. 
1. [ ] Select an onboarding buddy on the new team member's team - be sure to select someone in a similar time zone, who will not be on PTO in the first two weeks of onboarding and who has been with GitLab for more than three months. Review [Onboarding Buddies Handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) for more information around the program.
1. [ ] Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been selected, including a link to this onboarding issue and the aforementioned handbook page.
1. [ ] Assign the onboarding buddy to this issue by clicking Edit in the Assignees section (right-hand column) and enter the buddy GitLab handle. Please also update the onboarding `__BUDDY_HANDLE__` in the introduction section of this issue for ease of reference by the new team member.
1. [ ] The default laptop for the new team member will be an Apple MacBook Pro. If the new team member could benefit from using a Dell with Linux instead, remind the new team member to request it. Please remind your new team member, if they prefer using Linux, to follow the instructions in their onboarding task and choose full disk encryption when installing their operating system. Refer to the handbook for more information on [approved laptops](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptop-configurations).
1. [ ] Search in the [laptop order issue board](https://gitlab.com/gitlab-com/business-technology/team-member-enablement/it-equipment-order-processing/-/issues) for the new team member and confirm the status. If the laptop would not be available in their first day, coordinate with the new team member for an [exception](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#exception-processes). If any questions, reach out to `#it-help` for further assistance.
1. [ ] Schedule a Zoom video call with the new team member for the start of their first day to welcome them to the team. Send them an invitation to their personal email as their GitLab email address will only be activated on their first day.
1. [ ] Send an email to the new team member's personal email address welcoming them to GitLab. Provide them with the time and Zoom link to the introduction video call as well as any other information you feel will help with a great first day experience.
1. [ ] If the new team member has a LinkedIn profile, share this link with the rest of the team via Slack, encouraging them to reach out to the new team member to start building collaboration and excitement.
1. [ ] Review [GitLab's Probation Period Process table](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#current-locations-with-probationary-periods) and check if your new team member falls within a country that has a probation period. This is important as the People Connect team will be following up with you to review the probation period, within the period on the linked page. <!-- Intern: Remove -->
1. [ ] Review the role-specific tasks at the bottom of this issue, completing any manager tasks and coordinating with People Connect if the task list needs to be updated.
1. [ ] If this team member will be a People Manager, please note that the Interview Training and Becoming a Manager issues will be [opened](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#create-interview-training-issue) automatically, one week after their start date to make it less overwhelming their first week. <!-- Intern: Remove -->
1. [ ] Add [Manager:: Pre Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks. **Please only change the labels for `Manager` as other [labels](https://docs.gitlab.com/ee/user/project/labels.html) are used to track which tasks may still be incomplete for the People Connect Team.**


</details>

<!-- Intern: Update to Mentor -->
<details>
<summary>Buddy Tasks</summary>

1. [ ] Check your Calendar! If you will be taking extended PTO or going out on leave during the new team members first two weeks, please alert the manager that nominated you and ask for a new onboarding buddy to be assigned.
1. [ ] Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) to become familiar with your role in supporting the new team member.
1. [ ] Ask the new team members Manager for their personal email address either via email or direct message - once you have this feel free to reach out and introduce yourself using this [email template](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#example-email-template) as a guideline.
1. [ ] Set a reminder to schedule a Zoom call on the team member's first day to introduce yourself and ensure they know that you are on hand to answer any questions they may have. **Kindly note that their GitLab Gmail Account will not be active until their first day so any invitations you send prior to this should be routed to their personal email account instead**.
1. [ ] Schedule at least one follow-up chat in their second week or third week at GitLab. During that chat, offer to help them with updating the team page when they hit that task on the onboarding. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.
1. [ ] Check in with the new team member and see if they need help scheduling any coffee chats.  


:tada: Thank you for being an onboarding buddy! Don't forget that there is a quarterly onboarding buddy raffle where all buddies stand a chance to win a discount code to the swag store. Give it your best!

</details>

<details>
<summary>People Connect Tasks</summary>

**Issue Review**
   1. [ ] Ensure this issue is assigned to the People Connect team member and the hiring manager and check that the following information is accurate:

| Information | Value |
| :---------- | :---- |
| BAMBOOHR_ID | `__BAMBOO_ID__` |
| EMPLOYEE_NUMBER | `__EMPLOYEE_NUMBER__` |
| MANAGER | `__MANAGER_HANDLE__` |
| PEOPLE CONNECT SPECIALIST | `__PEOPLE_EXPERIENCE_HANDLE__` |

**Workday**
   1. [ ] If the new team member is listed as a referral, ensure that the `Referred by` field in the Workday Pre-Hire profile is filled in with the correct team member.
   1. [ ] Confirm that if the new team member listed any exceptions to the IP agreement in their contract, that the [approval process](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#reference-check-forms) has been completed  and documentation has been uploaded to the document category of IP Exception Agreement.
   1. [ ] If the new team member is a people manager, reach out to the PBP to learn and confirm the sup org name. 
      `We have a new people manager starting (i.e. Johnny Walker) on 20XX-XX-XX, what should the sup org name be?`
   1. [ ] Once you have the name of the sup org post in the `people-ops-tech` channel `Please can you create a sup org for NAME effective 20XX-XX-XX called (SUP ORG NAME)` 
   - If the manager is inheriting the sup org create an issue [here](https://gitlab.com/gitlab-com/people-group/people-tools-technology/general/-/issues)    

**BambooHR**
1. [ ] Ensure the new team members Gitlab email address and hire date match what is in Workday.

**Communication**
1. [ ] Add the following comment to the issue and tag the manager.
> I have created the onboarding issue; inside this issue, there are tasks for you and the onboarding buddy (you will assign). A portion of those tasks must be completed before the start date to ensure a successful onboarding process. We also suggest allowing for the new team member to have two full weeks to focus only on this onboarding issue. Please let me know if you have any questions.
1. [ ] Review the Onboarding Buddy tasks have been completed, if not send a friendly ping in this issue.
1. [ ] Review the Manager Pre-Start tasks have been completed, if not send a friendly ping in slack.
1. [ ] Add [PE: Pre-Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks.

**Laptop**
1. [ ] Check the [US laptop ordering form](https://docs.google.com/spreadsheets/d/1YmcEyoTDBkU01YtHF7kUHEhbrXlxUuAEKUaP2E5DvFM/edit#gid=796929635) or the [APAC/EMEA laptop ordering form](https://docs.google.com/spreadsheets/d/1RzYEkSOsgXFkh9KSoJcy2ZRV-kuso4ahYo6ajIQ--RU/edit#gid=1441676752) to see if the team member will be working on a Linux machine. 
   - If yes, add the `Encryption:: Missing` label to this issue to serve as a reminder that we need to check for the encryption screenshot. 
   - If no, then no action is needed.


</details>

---

### Accounts and Access

<details>
<summary>People Connect Tasks</summary>

#### Day 1

**Calendars & Agenda**
1. [ ] Add the new team member to the next [CEO AMA with New Hires](https://about.gitlab.com/culture/gitlab-101/) in the GitLab Team Meetings calendar for their relevant region APAC/EMEA/US team member. When prompted to "Edit recurring event", choose "This event".
1. [ ] Add the new team member to the next Onboarding Check-In Call for their appropriate timezone - select send notification.
1. [ ] Add the new team member as a Manager to the [GitLab Unfiltered YouTube account](https://myaccount.google.com/u/3/brandaccounts/103404719100215843993/view?pli=1) by clicking "Manage Permissions" and entering their GitLab email address.
1. [ ] Log into [1Password](https://gitlab.1password.com/people) and resend an invite to the new team member's GitLab email address.
1. [ ] Confirm that sign on bonus was processed, if applicable
1. [ ] Add [PE: Day 1- Complete Label](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Compliance
**After Day 1**
1. [ ] Verify that the new team member has checked the New Hire Security Orientation video training task (Day 1 Morning - Security Tasks).
1. [ ] Important: Verify the new team member has completed their onboarding tasks in Workday by confirming that the following fields have been populated: 
   - [ ] All Personal Details including Legal Name, Date of Birth, and National ID
   - [ ] Home Contact Information (**Important Note:** If the Home Contact Location doesn't match the Work Contact Location, you'll need to investigate and escalate to Total Rewards/CES if necessary)
   - [ ] GitLab Username
   - [ ] Benefit Group, if applicable
   - [ ] Emergency Contact Details
   - [ ] Confirm Start Date
      - If the start date in the signed contract doesn't match the date in Workday, check to see if a start date change was uploaded
      - If there is no start date change communication, reach out to the CES team to retrieve and have it uploaded
   - [ ] All Onboarding Acknowledgements 
1. [ ] Please ping the team member if their Workday Onboarding is not 100% completed by the end of Day 2. 
1. [ ] `LINUX COMPUTERS ONLY:` Check to ensure encryption screenshot has been uploaded and an encryption label (`encryption_missing` or `encryption_complete`) has been added to the issue. If it hasn't been added, please ping the team member in the onboarding issue to please complete the task.

**After One Week**
1. [ ] Add a comment to this issue, checking in on the new team member. Check on their progress within the issue.
1. [ ] If the new team member is a Manager, ensure that the `Becoming a Manager` and `Interview Training` issues have been opened and automatically assigned to the team member.

</details>


<details>
<summary>Manager Tasks</summary>

#### Day 1

1. [ ] Invite the team member to recurring team meetings, consider pressing `don't send`, this will reduce noise in their inbox on day 1.
1. [ ] Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with the team member. 
1. [ ] Set the team member's GitLab group and project-level permissions as needed.
1. [ ] Ensure the team member knows which individuals they will be collaborating with and working with on a day-to-day basis in their functional groups.
1. [ ] Introduce the new team member in the next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.
1. [ ] Introduce the new team member in important team and functional channels, so that colleagues outside of their immediate team can welcome them to GitLab.
1. [ ] Encourage the team member to join the monthly [`Let's Connect - GitLab Social Call`](https://about.gitlab.com/handbook/communication/#social-call) to stay connected with the wider team.
1. [ ] Add [Manager: Day 1 Complete Label](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Day 2

1. [ ] A [role based entitlement Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#role-entitlements-for-a-specific-job) will be created automatically for new team members **if** a [pre-approved template exists](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) and you will be tagged in it automatically. The AR will also be automatically added to the new team member epic together with their onboarding issue. The AR should request access to anything NOT provided by [baseline entitlements for all GitLab team members](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#baseline-entitlements-all-gitlab-team-members).
    - As the manager, please check if the AR covers the correct entitlements.
    - **Please note that in order to meet our SOX regulatory obligations any access to [SOX systems](https://internal-handbook.gitlab.io/handbook/finance/sox-internal-controls/it-general-controls/#2-gitlab-sox-in-scope-applications) with SOX-impacting user roles you will need to open an AR since this access cannot be included in role based entitlement templates.**
    - If the AR was not created AND a role baseline entitlement exists for the role, please create an [issue for People Ops Engineering](https://gitlab.com/gitlab-com/people-group/people-operations-engineering/-/issues/new)  and proceed with manually creating the AR for the new team member. If creating it manually, don't forget to include the [person's details](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request.md) in the issue.
    - For all other roles that do not have a role-specific AR template, open a [Individual or Bulk Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request). We recommend [creating a role based entitlements template](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#how-do-i-create-a-role-based-entitlement-template) so all future team members with a role get the AR created automatically.


#### Week 2

1. [ ] If this team member will be assigned to one or more [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages), and have successfully added themselves to the team page:
    1. [ ] Add a new entry for the team member in the [_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/includes/product/_categories-names.erb) file.
1. [ ] Open any role based onboarding issue for the team member to start working through, whilst also still completing this issue.

</details>

<details>
<summary>Accounts Payable Tasks</summary>

#### Day 1

1. [ ] Accounts Payable (@accounts-payable): If an applicable policy exists for the employee type and entity, [invite team member](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#add-expensify) to an account in [Expensify](https://www.expensify.com/signin). For contractors, search to see if there is an `iiPay` policy in the currency of the team member's compensation, and invite to that policy if it exists. If none currently exists, create a new policy and the new team member to that policy.

</details>

<details>
<summary>Legal Tasks</summary>

#### Day 1

1. [ ] Legal (@ktesh, @laurenlopez): Assign training in Navex LMS.

</details>

---

<!-- include: country_tasks -->

---

<!-- include: exemption_tasks -->

---

<!-- include: entity_tasks -->

---

### Day 1 - Getting Started: Accounts and Onboarding

**Time to jump in and begin your team member journey! Your first day will include critical tasks for completing onboarding paperwork, security training, and getting your core systems in place.**

<details>
<summary>New Team Member</summary>

### :red_circle: Okta

1. [ ] :red_circle: If you haven't already, please set up your Okta Account. GitLab uses Okta as its primary portal for all SaaS Applications. You should already have an activation email in both your Gmail and Personal Accounts. Okta will populate your Dashboard with many of the Applications you will need, but some of these will require activation. Read and Review the [Okta handbook page](https://about.gitlab.com/handbook/business-technology/okta/) for more information. Please enable Touch ID (required for Mac users) or enroll a Yubikey (required for Linux users).
   1. [ ] Please use Google Chrome on your laptop as our applications are set up in Google environments. Using Safari (Apple users) can hinder your ability to set your device up correctly.
   1. [ ] Please login to your Gitlab account via Okta. Do this by clicking the GitLab.com tile directly from your Okta dashboard. **Note: the GitLab tile can take 24-48 hours to appear in Okta. If this does not appear after this time period, please reach out to [`#it_help`](https://gitlab.slack.com/messages/it_help) on Slack**
   1. [ ] If you created any accounts while onboarding before being added to Okta, make sure that you can also SSO into these accounts by clicking on the relevant tile in Okta.
   1. [ ] Update any existing passwords for GitLab systems in line with the security training and [guidelines](https://about.gitlab.com/handbook/security/#accounts-and-passwords).  

### GitLab Onboarding Issue & Apple IDs

1. [ ] Assign the onboarding issue to yourself. Click `Edit` in the Assignees section (right-hand column) and enter your GitLab handle. 
1. [ ] If you are already in possession of a company-owned Apple laptop, please create or update the Apple ID, using your GitLab email address. **Note:** You will need access to your GitLab Gmail inbox to verify your Apple ID. Your GitLab Gmail inbox will be accessible after setting up Okta (refer to the Okta Task below).
   - If you have [difficulty creating an apple ID from system preferences](https://discussions.apple.com/thread/8438000), try creating it through the [App Store](https://support.apple.com/en-us/HT204316).
   - If you're trying to login to the App Store and getting stuck in a "Review" & login infinite loop, try signing in via https://appleid.apple.com and updating your payment info, then login via Apple Music. You should hopefully be able to login to the app store now. Reach out to [`#it_help`](https://gitlab.slack.com/archives/CK4EQH50E) if you're still having issues.
   - If an app is critical to your work, it can be reimbursed through Expensify. If your laptop is of a different make, please assign any computer or store ID to your GitLab email address.
   - If you have not received your laptop yet, some of the tasks below (including ones marked with the red circle :red_circle:) won't be actionable - this is okay, just make sure your manager is aware and make a note to remind you to complete the incomplete tasks once you receive the laptop! 
1. [ ] Submit [this form](https://docs.google.com/forms/d/e/1FAIpQLSddexI8VZTCiyxme1_7QtbQZ6WoIJRlHdaI2Gi6PD8Eti-DLQ/viewform) to request the Ultimate tier for your GitLab.com account. Filling out this form will convert your GitLab.com account to the Ultimate tier, so you can use all features without adding a credit card. See [the Incentives page on the Handbook](https://about.gitlab.com/handbook/incentives/#gitlab-ultimate) for more details.
 
### :red_circle: 2FA (Two-Factor Authentication)

***This next task must be done within 48 hours or you will be locked out and need to wait for IT Ops to help you get reconnected.***

1. [ ] GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication). You sign in with something you know (your password) and something you have (a code you can copy from your Virtual Multi-Factor Authentication Device, such as 1Password or Google Authenticator). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically").
    - If you have problems with 2FA just let IT Ops know in the [`#it_help`](https://gitlab.slack.com/messages/it_help) Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling. It's better than getting locked out and needing to wait for IT Ops to help you get back online.
1. [ ] :red_circle: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com
account. Please enable both TOTP and WebAuthn (Yubikey/biometric).
**If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.**
1. [ ] :red_circle: MacBooks (required) - Enable WebAuthn/TouchID/FaceID to log in via Okta 
   - [ ] Enable Touch ID for your MacBook, please follow the `Set up Touch ID` instructions in [this guide](https://support.apple.com/en-ca/guide/mac-help/mchl16fbf90a/mac).
   - [ ] Enable TouchID for your Okta account using the [handbook instructions](https://about.gitlab.com/handbook/business-technology/okta/#i-want-to-add-touch-id--face-id--yubikey-to-okta) 
   - [ ] Go to your [settings in Okta](https://gitlab.okta.com/enduser/settings) > scroll down to `Security Methods` > ensure that `MacBook Touch ID` is set up under `Security Key or Biometric`. 
1. [ ] :red_circle: Linux (required) - Enroll a Yubikey
   1. [ ] Please fill our [this form](https://forms.gle/VZ7Q4RWXfmfB58FJ9) for us to procure te YubiKey for you.
   1. [ ] Follow instructions [here](https://about.gitlab.com/handbook/business-technology/okta/#i-want-to-add-touch-id--face-id--yubikey-to-okta) to set up your YubiKey.
1. [ ] Okta on your phone/tablet (optional, but strongly recommended as a backup)
      - Visit https://gitlab.okta.com from your phone or tablet's web browser.
      - Follow the [handbook instructions](https://about.gitlab.com/handbook/business-technology/okta/#i-want-to-add-touch-id--face-id-to-okta-for-my-mobile-device-iphone-android-tablet) to add a biometric device. 
      - iPhone - You will be prompted to enable FaceID/TouchID. The Apple ID does not need to be the same as your GitLab account.
      - iPad - You will be prompted to enable FaceID/TouchID.
      - Android - Enroll with biometrics. If your device does not support biometrics, you will temporarily lose access to Okta from this device while we explore options.

### :red_circle: 1Password

1. [ ] :red_circle: Install 1Password. GitLab provides all team members with an account to 1Password. Some teams use 1Password vaults for shared notes and some shared accounts. Should your team have a vault, this will be added to your access request issue. 1Password is available as a standalone application for macOS or as a browser extension and CLI tool for Linux. 
   1. [ ] Accept the 1Password invitation found in your GitLab email inbox.
   1. [ ] Install the 1Password app or browser extension on your computer. Then link it to your team (GitLab company) account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Tip: Please use Google Chrome on your laptop as our applications are set up in Google environments. 
1. [ ] Read 1Password's [getting started guides](https://support.1password.com/explore/extension/).
1. [ ] Read [Get to know 1Password for Mac](https://support.1password.com/getting-started-mac/) to understand 1Password's Mac functionality.
1. [ ] Before saving passwords in 1Password, understand why some will be in 1Password versus Okta. Passwords should be saved in your [*Private* 1Password vault](https://support.1password.com/1password-com-items/) if they cannot be saved in Okta. This will make the passwords only accessible to you. If you save passwords in a shared vault they will be accessible to others at GitLab.
1. [ ] Confirm in the comment box that you are using 1Password and Okta in accordance with our Security Practices.

### :red_circle: Security Tasks

1. [ ]  :red_circle: Login to your ProofPoint account via Okta. Do this by clicking the ProofPoint tile directly from your Okta dashboard. This will create your account in [ProofPoint](https://gitlab.ws01-securityeducation.com/). An email will be sent within 15 mins assigning New Hire Security Training to you. Training must be completed by end of Day 1.
1. [ ] :red_circle: Watch the required [New Hire Security Orientation](https://gitlab.ws01-securityeducation.com/) training.
1. [ ] :red_circle: Add a comment with a screenshot of your training completion certificate. Unfortunately, you are unable to go back at a later time to download the certificate. The only option is to print the certificate when it is displayed.
1. [ ] :red_circle: Confirm you have [PhishAlarm](https://about.gitlab.com/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack) in your Gmail sidebar.
1. [ ] :red_circle: Hard Drive Encryption
   - **If you are a Mac user, check the above checkbox off as a form of acknowledgement.**
   - `LINUX COMPUTERS ONLY:` Instructions for Linux set-up process [here](https://about.gitlab.com/handbook/tools-and-tips/linux/#initial-installation).
1. [ ] :red_circle: Install DriveStrike. 
   - **If you are a Mac user, check the above checkbox off as a form of acknowledgement.**
   - `LINUX COMPUTERS ONLY:` Please reach out in the #it_help Slack channel to request DriveStrike. Leave a comment in this issue with a screenshot of successful installation. Information about DriveStrike and the use of can be found [here](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#fleet-intelligence--remote-lockwipe). 
1. [ ] :red_circle: Leave a comment in this issue with a screenshot to verify that your hard drive is encrypted. Be sure to include the serial number of your computer for correlation. **If you are a Mac user, please still check this box off as a form of acknowledgement**
   - **If you are a Mac user, check the above checkbox off as a form of acknowledgement.**
   1. [ ] `LINUX COMPUTERS ONLY:` Please review the [Full Disk Encryption](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#full-disk-encryption) page in the handbook for the appropriate steps to create the screenshot with the necessary information.
   1. `Prt Scrn` will take a screenshot of the entire desktop; `Alt + Prt Scrn` will take a screenshot of a specific window and `Shift + Prt Scrn` will take a screenshot of an area you select.
   1. [ ]  To Upload the screenshot to your issue: Find the `Attach a file` link at the bottom of this issue and press Comment. Depending on the operating system you are using, the steps to retrieve this information may be different.
***Please note that GitLab's [Internal Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#unacceptable-system-and-network-activities) prohibits the use of tools that capture and share screenshots to hosted third-party sites so please make sure that you do not leverage such a tool to complete this step.***
1. [ ] :red_circle: Jamf enrollment - *Mac only*
    - If you have self-procured your machine, please make sure to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf).
    - If the company has procured the machine for you, please confirm that [Jamf is installed correctly](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#installation-completion-confirmation). If not, please follow the steps to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf). Please ask any questions in the `#it_help` Slack channel.

### Getting Started with Slack

1. [ ] Using your @gitlab.com email, register on Slack by following this [invitation link](https://join.slack.com/t/gitlab/signup). Read the next suggestions on how to choose a username first.
1. [ ] Pick your [Slack username](https://gitlab.slack.com/account/settings#username) so that others are able to identify you, that's why your full name is preferred.
1. [ ] You have been automatically added to a few key Slack channels. Check out all of GitLab's [key Slack channels](https://about.gitlab.com/handbook/communication/#key-slack-channels) to get started. You can also reference the Slack help center for [instructions on browsing all available channels](https://slack.com/help/articles/205239967-Join-a-channel); we have thousands!


### :red_circle: Workday

***This section is time sensitive in ensuring your details are added to our People Team and Payroll systems.  If you happen to get stuck, contact us in Slack via #people-connect or email people-connect@gitlab.com.*** 

Workday is our HRIS (Human Resource Information System) for all team members. Through our self-service model you can access and update your personal details, view your job details, or access your employee documents.

**Accessing Workday**

Note: Chrome is the best recommended browser to use with Workday. 
Navigate to your [Onboarding Dashboard](https://www.myworkday.com/gitlab/d/task/14860$26.htmld) or from your Okta dashboard, locate and click the ‘Workday’ icon. 

**Complete Your Onboarding**

 1. [ ] :red_circle: Please complete all onboarding tasks in your Workday inbox, requirements vary based on your work country. Workday follows a step-process, be sure to submit all lingering inbox items to receive the next set of steps. It is required to complete 100% of your onboarding within your first couple of days. Please prioritize and check your status through your progress indicator on your dashboard. (Please ensure that your address and Emergency Contact information is entered using the [Latin / Roman alphabet](https://en.wikipedia.org/wiki/Latin_alphabet), since GitLab communicates in English using this alphabet.)
 1. [ ] :red_circle: Review and acknowledge any company or work state/country specific policies. You can access your acknowledged documents at any time by typing "Maintain My Worker Documents" using the search bar in Workday.

### BambooHR

1. [ ] BambooHR is a People Team tool that is used to track time off/absence requests. If you are a contractor, this system will also be used to submit your invoices. Access BambooHR from your Okta account dashboard.

### :red_circle: Payroll Info
1. [ ] :red_circle: Review Payroll Information for [US team members](https://about.gitlab.com/handbook/finance/payroll/#us) or [non US team members](https://about.gitlab.com/handbook/finance/payroll/#non-us) page based on your work location this will include information around pay dates, cut-off dates for expensing purposes etc.

### GitLab Gmail
1. [ ] Add your photo to your Google/Gmail Account Profile as is company policy this should be an unobscured photograph of yourself and not an avatar or stock image.
1. [ ] Set up your email signature using the following [example as a guideline](https://about.gitlab.com/handbook/tools-and-tips/#email-signature).

### Complete Your Slack Profile
1. [ ] Fill in your [Slack profile](https://gitlab.slack.com/account/profile), as we use Slack profiles as our Team Directory to stay in touch with other team members. The fields include:
    1. [ ] Photo
    1. [ ] What I Do (can be your job title or something informative about your role)
    1. [ ] Time Zone (useful for other GitLab team members to see when you're available)
    1. [ ] GitLab.com profile, set the Display text to your `@gitlabusername`
    1. [ ] State / Province and Country (Optional)
    1. [ ] Working hours, can help others to identify the times you are generally available
    1. [ ] GitLab Birthdays: Opt in or out of the birthday celebrations by selecting `yes` or `no`
    1. [ ] If you'd like to, you can add your personal email address, phone number including country code (this is optional)
    1. [ ] Consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
    1. [ ] Consider adding your pronouns in the `Pronouns` field. By making it normal to set pronouns we create a more inclusive environment for people who use non-traditional pronouns.
    1. [ ] Consider adding pronunciation guides for your full name in the `Pronunciation Guide` to help others to pronounce your name correctly (e.g. sid see-brandy for Sid Sijbrandij).
    1. [ ] Do not enter anything in the `G-Cal Booking!` field unless you would like to set up and make use of a [Calendly Account](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#calendly)
1. [ ] Read our handbook section on [communication via Slack](https://about.gitlab.com/handbook/communication/#slack), paying particular attention to the item concerning `Please avoid using @here or @channel unless this is about something urgent and important`.
1. [ ] Sign up to take the [Communicate at GitLab: Slack training](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/slack-training/). This training is tailored to how we use Slack at GitLab and is meant to help you learn norms and expectations set in the [Slack handbook](https://about.gitlab.com/handbook/communication/#slack).
1. [ ] Introduce yourself in the Slack [`#new_team_members`](https://gitlab.slack.com/messages/new_team_members/) channel, where you can ask any questions you have and get to know other new team members! Tips:
   1. Make sure your Slack profile has a **photo** - it makes it easier for other GitLab team members to remember you!
   1. We love to hear more, such as where you were before, family, pets, and hobbies.
      - For example: _Hi, My name is `<your name>`, I am starting as a `<description of your role>` in the `<name of your team>`. I live in `<city/locality/state>`, `<country>` and was most recently `<description of what you did before joining GitLab>`. Outside of work I like to `<tell us a bit about what you enjoy / hobbies>`_ 
   1. We also enjoy pictures if you're willing to share. Consider giving your new team members a glimpse into your world. You can scroll through previous messages in the channel for inspiration.


### Zoom
1. [ ] Set up your [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/zoom/#zoom-setup). Your Zoom account will be set up via [Okta for SSO Login](https://gitlab.okta.com/app/UserHome), so log in using the Zoom SSO tile.
1. [ ] Fill in your [profile](https://zoom.us/profile) with your Name, Job Title and Profile Picture. Since Zoom doesn't display the job title field during meetings, it is recommended that you [add your job title](https://about.gitlab.com/handbook/tools-and-tips/zoom/#adding-your-title-to-your-name) as part of the display name field e.g. if your name is `John Appleseed` and your role is `Engineer`, you can compile your display name as follows `John Appleseed - Engineer`.
   1. [ ] Consider adding your pronouns in the `Pronouns` field. 
      1. Below this, there is `How would you like to share your pronouns?`. Select the option that works best for you.
      1. Click `Save` and your pronouns should now be visible on Zoom.
1. [ ] Consider setting your default recording view as per the following:
   1. Go to the zoom.us web application and log into [your profile](https://zoom.us/profile).
   1. Click the `Settings` tab on the left, then [the `Recording` tab](https://zoom.us/profile/setting?tab=recording).
   1. Make sure you have `Record active speaker with shared screen` selected. Remember to save.
   1. _Mac only_ - in case you are not able to share the screen in Zoom desktop client (as it is disabled by default), check this link [Permissions for the Zoom desktop client](https://support.zoom.us/hc/en-us/articles/360016688031) how to fix the issue (please note, the permission may not appear under _Security & Privacy_ until after you've run the Zoom app for the first time).
1. [ ] Consider adding a Virtual Background image by following [these steps](https://about.gitlab.com/handbook/tools-and-tips/zoom/#virtual-background) from our Handbook. 
   1. You can access a number of custom GitLab backgrounds from our [assets page](https://drive.google.com/drive/u/0/folders/1Fv6_e_1dgSDE5N_KuMvtDM6gdNIUgRcT) 
1. [ ] Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.
1. [ ] :red_circle: Enable the ability to share your screen for your Zoom app on your MacBook.
   1. On your MacBook, open `System Preferences` > `Security & Privacy` > `Privacy` tab.
   1. Click on the :lock: and enter your password to your MacBook to unlock it.
   1. On the left hand side of this window, scroll down and click on `Screen Recording` > select `zoom.us` on the right hand side.
   1.  Click on the :lock: again to apply the changes. You will need to quit and re-open the Zoom app.
1. [ ] :red_circle: Sign into your Zoom app on your MacBook via `SSO`. You can follow this [guide](https://support.zoom.us/hc/en-us/articles/201800126-Signing-in-with-SSO-) under `Zoom desktop client`.


#### Google Calendar Set Up
1. [ ] Read our handbook section on [Google Calendar](https://about.gitlab.com/handbook/tools-and-tips/#google-calendar)
1. [ ] Set your Google Calendar [default event duration](https://calendar.google.com/calendar/r/settings) to use `speedy meetings`
1. [ ] Consider allowing calendar event [guests to modify events](https://about.gitlab.com/handbook/tools-and-tips/#modifying-events) for meetings you set up. The ability for guests to modify events speeds up the ability for guests to contribute to event times without the need to merely propose a new time.
1. [ ] Verify you have access to view the GitLab Team Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to  calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com` and then press enter on your keyboard. Reach out to #people-connect via slack if you have any questions. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar.
1. [ ] Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings. However, even private meetings are viewable by your manager and those who manage them.
1. [ ] [Set your working hours & availability](https://support.google.com/calendar/answer/7638168?hl=en) in your Google Calendar.


</details>

---

### Day 2 - Remote Working and our Values

**Welcome to Day 2 of onboarding! Today you'll learn more about our CREDIT values and ways of working along with exploring Level Up.**

_The Learning and Development team is currently transitioning to Level Up, our new learning platform. If you find you cannot access the Level Up links below, please first check in OKTA and access directly there. If you still cannot access, check again on your 3rd day at GitLab. If on the start of your 3rd day you still cannot access, please reach out in the #learninganddevelopment Slack channel._

<details>
<summary>New Team Member</summary>


### Level Up
Most onboarding trainings will be hosted in [Level Up](https://levelup.gitlab.com/internal-team-members). Today, you'll begin to explore training material in the Level Up, our Learning Experience Platform for team members, customers, and community members.

1. [ ] You can access Level Up via SSO in OKta.
1. [ ] Complete this [Learning Style survey](https://forms.gle/SqrT1fCuE4VNfmyZA) and tell us how you prefer to learn so we can improve the Level Up experience.

### Remote Work
1. [ ] It can take some time adjusting to our remote work style, particularly if you're new to working in a 100% remote environment. Read our [Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). You'll find other tips for operating and thriving in an all-remote setting within the [All-Remote section of our handbook](https://about.gitlab.com/company/culture/all-remote/). Take some inspiration from our team members on how they structure their remote working day by reading this helpful blog post: [A day in the life of remote worker](https://about.gitlab.com/blog/2019/06/18/day-in-the-life-remote-worker/).
1. [ ] Learn more about the all-remote onboarding process and [best practices](https://about.gitlab.com/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
1. [ ] Complete first two lessons of [Remote Work Foundations Certification](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/remote-foundations) found in Level Up. Please note you do not need to complete the whole training, as you will complete portions throughout your onboarding. These two lessons will set you up for success during your first few days at GitLab.

### Communication
1. Clear, considerate communication is especially important at an all-remote company. Review the next four assessments within the [Remote Work Foundation Certificate](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/remote-foundations).
1. [ ] Familiarize yourself with our [Communication page](https://about.gitlab.com/handbook/communication/) and learn about why it is important and how to identify personally identifiable information (PII) and the [confidentiality levels](https://about.gitlab.com/handbook/communication/confidentiality-levels/) of private vs public data.

### Values
1. [ ] [GitLab values](https://about.gitlab.com/handbook/values/) are documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business. Take and pass our [Values Certification](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-values) to become certified in our GitLab Values.

### Mission
1. [ ] Gitlab believes in a world where everyone can contribute. Take some time to [look over our mission](https://about.gitlab.com/company/mission/) to see how we plan to put this into action.

### Workspace <!-- Intern: Remove -->
1. [ ] Take some time to consider the set up of your workspace. Check out these helpful Handbook pages on how to [Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace). Having an ergonomic and comfortable workspace will enable the utmost productivity.
1. [ ] Review the [Spending Company Money Guidelines](https://about.gitlab.com/handbook/spending-company-money/#guidelines) and the [Office Equipment and Supplies Averages](https://about.gitlab.com/handbook/finance/expenses/#office-equipment-and-supplies).
1. [ ] You are able to [expense](https://about.gitlab.com/handbook/finance/expenses/#-reimbursements) your monthly Internet subscription. If you have a total cost package with your provider (telephone, Internet, cable), be sure to calculate only the cost of the Internet portion and expense that.
1. [ ] If you have any questions around the expensing process be sure to reach out to Accounts Payable via the #expense-reporting-inquiries channel in Slack or via ap@gitlab.com 

### Update Your LinkedIn Profile (Optional)

1. [ ] Consider updating your LinkedIn account to feature your new role at GitLab and connect it to the GitLab page. You'll know this worked when you see the GitLab logo next to your current role.
1. [ ] If you would like to update your LinkedIn banner to feature GitLab consider making use of the [following image](https://gitlab.com/gitlab-com/people-group/talent-brand-and-engagement/-/blob/master/Social%20Media%20banners/LinkedIn_Banner-1400x300.png) - this is a great way to visually associate yourself with GitLab or a subject matter area you're passionate about.
1. [ ] Learn more about how to become a GitLab brand ambassador (https://about.gitlab.com/handbook/hiring/gitlab-ambassadors/#1-optimize-your-social-media-profiles-especially-linkedin) 

#### Events
Your calendar includes invitations to the following meetings. They are optional, but you are highly encouraged to attend. Please note: These meetings may not fall during your first week.

1. [ ] If you have questions getting your work station set up consider joining the weekly [IT Assistance w/ Onboarding](https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=NGY5ODg2ZzRoZWFnZDQ2NWx2MDFwcjZ0M2JfMjAyMjExMTVUMTcwMDAwWiBqd29uZ0BnaXRsYWIuY29t&tmsrc=jwong%40gitlab.com&scp=ALL). This is an optional live troubleshooting session and space for you to ask questions about anything IT related.
1. [ ] On the GitLab Team Calendar, watch for the Security Group Conversation, Show & Tell, and Security Office Hours. During these Zoom calls, you can ask any security-related questions and see updates from the Security team. Join the [`#security`](https://gitlab.slack.com/messages/security) Slack channel to ask any questions you may have asynchronously.
1. [ ] In the [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/) calendar description, find the call agenda link where all team members can insert questions for the CEO. Tip: Videos of previous CEO 101 sessions within the GitLab archives can help you determine questions to contribute.

### Helpful Links and Tools

Feel free to come back to this section when it's helpful for you!

1. [TaNEWki Tips](https://about.gitlab.com/handbook/people-group/general-onboarding/tanewki-tips/) useful information to support your onboarding journey.
1. [ ] Access GitLab Internal Handbook via SSO in OKta.
1. [ ] If you have questions about how to use and search the handbook [this page](https://about.gitlab.com/handbook/handbook-usage) will provide you with a full overview.
1. You can find your respective departments section of the handbook using the following [index](https://about.gitlab.com/handbook/).
1. [Select and Speak - Text to Speech](https://chrome.google.com/webstore/detail/select-and-speak-text-to/gfjopfpjmkcfgjpogepmdjmcnihfpokn?hl=en)
1. [Read Aloud: A Text to Speech Voice Reader](https://chrome.google.com/webstore/detail/read-aloud-a-text-to-spee/hdhinadidafjejdhmfkjgnolgimiaplp?hl=en)
1. [Rectangle App](https://rectangleapp.com/) - Move and resize windows with ease
1. [OneTab](https://www.one-tab.com/) - Tab organizer for Chrome, Firefox, Edge, or Safari

### Your Role
- Spend the rest of your day focused on role-based tasks, some of which you will find at the bottom of this issue, others can be found in your role-based training issue which your manager will have opened and assigned to you.



</details>

---

### Day 3 - Security & Compliance

**Welcome to day 3! By now, you should have your systems in place, more familiarity with our values, and have accessed Level Up. Today, you will be going over the final Security and Compliance tasks within onboarding.**

<details>
<summary>New Team Member</summary>

### :red_circle: Security

1. [ ] Please add your serial code for your GitLab laptop here: [GitLab Notebook information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform?usp=sf_link). You will need the evidence of [full disk encryption.](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#full-disk-encryption)
1. [ ] Review the link to our internal phishing program documentation - (https://about.gitlab.com/handbook/engineering/security/security-assurance/governance/phishing.html)
1. [ ] Review how to [report phishing scams](https://about.gitlab.com/handbook/security/#contact-gitlab-security) and be aware that [CEO & Executive fraud](https://about.gitlab.com/handbook/security/#ceo--executive-fraud) via email and SMS is likely. 

### :red_circle: Legal
1. [ ] :red_circle: Set-Up your [NAVEX Global](https://gitlab.okta.com/home/gitlab_navexglobal_1/0oa5wjan9dTEiZd3L357/aln5wjjtvyPGkf5qn357) account and complete the trainings listed in there. You should log into NAVEX via Okta. Make sure you have disabled your pop-up blocker for NAVEX launch to work. If you experience any issues, reach out to the team in the Slack channel [`#compliance-training`](https://gitlab.slack.com/messages/compliance-training).
1. [ ] :red_circle: Complete [Regulation FD training](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-regulation-fd-training) in Level Up.
1. [ ] :red_circle: Review [GitLab's Anti Bribery and Corruption policy](https://about.gitlab.com/handbook/legal/anti-corruption-policy/). Mark this task as complete to acknowledge that you have read, understand and agree to abide by the policy.
1. [ ] :red_circle: Don't forget to comply with the contract you signed, and make sure you understand [confidentiality](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d) (see page 6 of the Code of Business Conduct and Ethics).

### :red_circle: Compliance
1. [ ] :red_circle: Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/). Pay particular attention to any country-specific requirements that relate to your location.
1. [ ] :red_circle: Please complete the [Harassment Prevention Training](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive) via WILL Learning within your first 30 days. If you did not receive an email invite, review the instructions and find the appropriate course link [on this handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive).
   1. [ ] **Important Note (Trigger Warning):** GitLab is sensitive to those who may have experienced harassment in the workplace previously and would like to remind new team members of the Employee Assistance Program we offer through [Modern Health](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/) if you would like someone to talk to.
1. [ ] :red_circle: Review and familiarize yourself with our [SAFE Framework.](https://about.gitlab.com/handbook/legal/safe-framework/)
1. [ ] :red_circle: Please read our [People Policies](https://about.gitlab.com/handbook/people-policies/) page. This contains important team member-related policies to ensure that we adhere with to required laws.


### Your Role
- Spend the rest of your day focused on role-based tasks, some of which you will find at the bottom of this issue, others can be found in your role-based training issue which your manager will have opened and assigned to you.

</details>

---

### Day 4 - Social & Benefits

**Welcome to Day 4! This day brings you to the social aspects of GitLab, as well as learning about and enrolling in benefits. You will learn about the team, how we connect to one another all over the world, and our benefits.**

<details>
<summary>New Team Member</summary>

### Get to Know the Team

1. [ ] Review the GitLab Organizational Chart. You can view the chart and structure [in the handbook](https://about.gitlab.com/company/team/structure/) or by going to Workday. From your Workday home page, select View All Apps - click the Directory worklet - select View - My Org Chart

**Coffee Chats**
[Coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) are how we get to know our team members. They are informal and casual.

During your first two weeks, schedule 5 calls with different team members. Team members are located all over the world so this is a great way to learn more about other countries or locations. See if you can connect with at least one person in another time zone and country that doesn't border your own.

Try to avoid sending invites for a Friday meeting. [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays) create a designated meeting-free space within our weeks for focused work.

Remember that as part of a geographically-diverse team, your Monday and Friday may also overlap with someone else's weekend.

Not sure where to start connecting? Below are some ideas to get you started.

- In support of GitLab's values of [Diversity, Inclusion & Belonging](https://about.gitlab.com/company/culture/inclusion/) scheduling calls with team members in other regions and countries.
- Ask your onboarding buddy or your manager.
- Consider joining the [Let's Connect - GitLab Social Call](https://about.gitlab.com/handbook/communication/#social-call).
- Check out the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week.
- If you enjoy traveling, check out the different [`#loc_` channels](https://about.gitlab.com/handbook/communication/chat/#location-channels-loc_).
- Join a `#lang-` channel if you are learning or interested in learning a language.
- Join a [Social Group](https://about.gitlab.com/handbook/communication/chat/#social-groups) channel in Slack to find like-minded team members that share your same interests, hobbies, and lifestyles.
  
We want to get to know you! There's no need to ask people if it's okay to schedule a call first – go ahead and schedule a meeting using the [Find a time](https://about.gitlab.com/handbook/tools-and-tips/#finding-a-time) feature in Google Calendar and introduce yourself in the invitation.


**Team Member Resource Groups (TMRGs)**
GitLab encourages team member participation in [Team Member Resource Groups (TMRGs)](https://about.gitlab.com/company/culture/inclusion/erg-guide/) which include [Minorities in Tech (MIT)](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/), [Women+](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/) and [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) amongst [others](https://about.gitlab.com/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels).

### Social Media and Connecting
**GitLab Profile**
1. [ ] Fill in your [GitLab.com profile](https://gitlab.com/-/profile), including: Organization = "GitLab", Job title = "(title)".
   1. [ ] Given our value of [transparency](https://about.gitlab.com/handbook/values/#transparency) and because GitLab is [public by default](https://about.gitlab.com/handbook/values/#public-by-default), ensure you do *not* use a [private profile](https://docs.gitlab.com/ee/user/profile/#private-profile), i.e. make sure that the checkbox under **Private profile** is unchecked.
   1. [ ] Consider adding your pronouns to the `Pronouns` field.
      1. Click the `Update Profile Settings` button at the bottom of the page to save these changes.
      1. Your pronouns will now be visible on your profile and when you hover over any @mentions.
   1. [ ] Add a picture to your GitLab profile. If you experience any difficulties with the upload, please try using another browser.
   1. [ ] Consider adding your pronunciation guide to the `Pronunciation` field for your full name to help others to pronounce your name correctly (e.g. sid see-brandy for Sid Sijbrandij).


**GitLab Social Media**
1. [ ] Consider connecting with or following GitLab's social media sites: [LinkedIn](https://www.linkedin.com/company/gitlab-com), [Twitter](https://twitter.com/gitlab), [Facebook](https://www.facebook.com/gitlab), and [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).
1. [ ] Learn about [Team Member Social Advocacy](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/team-member-social-advocacy/) and the available messaging in our [Bambu](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/team-member-social-advocacy/#bambu-our-social-advocacy-tool) tool.

**GitLab Unfiltered**
1. [ ] Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. ***If you do not accept this invite, you will not be able to watch the recordings on Youtube Unfiltered and we use this platform daily***. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
    1. When you accept the invite, you will see “GitLab Unfiltered” under “Your Brand Accounts” section.
    1. Please do not edit or change permissions of the “GitLab Unfiltered” account. This is a shared account for the company maintained by Marketing.
    1. Please do not update the image on this Youtube account as it is a shared account and the image should remain the GitLab brand image.
    1. To view GitLab Unfiltered content:
         1. Open a new browser tab and navigate to [YouTube](https://www.youtube.com)
         1. Ensure that you have **switched** your account to the GitLab Unfiltered profile
         1. Not sure if you have? Watch the [Unable to view a video on Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube)
    1. Confirm you have completed the above task, by watching [TEST Private Stream](https://youtu.be/gCyUdHixHN0).
    1. Unable to view the video? Go back to the above steps.
    1. After watching videos using the "GitLab Unfiltered" account, **switch back** to your own account.

**Moo Business Cards**
Please head to our [Frequently Requested Internal Handbook Page](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/frequently_requested/) for more information around ordering business cards and other items that may be necessary for your role.

### Benefits
1. [ ] If you are employed by one of our [entities](https://about.gitlab.com/handbook/people-group/employment-solutions/), view your entity's [benefits information](https://about.gitlab.com/handbook/total-rewards/benefits/).
1. [ ] If you are not already offered a GitLab life insurance plan through the [entity-specific benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/), fill in the [expression of wishes form](https://docs.google.com/document/d/19p4IN-msvs4H10teBessfPx-1wu8J7Vs67Es8iVLlL8/edit?usp=sharing). For your beneficiaries to have access to the benefit, you must indicate in writing to whom the money should be transferred. To do this, copy the template to your Google Drive (File > Make a copy), enter your information, and sign electronically. To sign the document, you can use one of the following methods:
   - Print, sign and digitize; or
   - Export to PDF (File > Download > PDF Document (.pdf)), and use one of the following tools to electronically sign the exported document:
     - In macOS, [use the native Preview app](https://support.apple.com/guide/preview/fill-out-and-sign-pdf-forms-prvw35725/mac).
     - In Linux, [use the Xournal app](https://www.xmodulo.com/add-signature-pdf-document-linux.html)'s [pen tool](http://xournal.sourceforge.net/manual.html#tools) to draw the signature.
     - Use an online tool like [eSign PDF](https://smallpdf.com/sign-pdf) (requires uploading the file) or [HelloSign](https://app.hellosign.com/) (requires an account). Please beware that there are always security concerns with tools that require uploading personal data. So, you may want to do this as a last resort only.
   - After signing, upload the PDF file to your Worker Documents by navigating to Workday > View all Apps > Personal Information > Worker Documents.  Once uploaded, select Employee Uploads as the Document Category.

**Time Off**
1. [ ] Review our [Flexible Paid Time Off (PTO) policy](https://about.gitlab.com/handbook/paid-time-off).
- GitLab values work-life balance, and encourages team members to have a flexible schedule and take vacations. You should not feel uncomfortable about taking time off. 
- Additional steps might be needed if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
1. [ ] Complete the [PTO training](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/pto-training-1) in Level Up.

**Mental Health Benefits**
1. [ ] GitLab offers an Employee Assistance Program to all team members, through Modern Health. Read more in the [Modern Health handbook page](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/) for more information on this amazing benefit. Your account may not be immediately available but will be within the first 14 days. New GitLab team members are included in weekly waves to Modern Health.
1. [ ] At GitLab, we strive to create a safe space for all team members, including those with mental health predispositions or neurological differences. Consider joining the [`#mental_health_aware`](https://gitlab.slack.com/messages/mental_health_aware), [`#neurodiversity`](https://gitlab.slack.com/messages/neurodiversity), and/or [`#diverse_ability`](https://gitlab.slack.com/messages/diverse_ability) Slack channels for more awareness/support on these topics.

**Equity & Compensation**
1. [ ] Read about GitLab [Equity compensation](https://about.gitlab.com/handbook/stock-options/). If you received Restricted Stock Units (RSU's) as part of your contract, your RSU's will be approved by the Board of Directors at their [quarterly board meetings](https://about.gitlab.com/handbook/board-meetings/#board-meeting-schedule). `E*Trade` is our system of record for equity, when your grant is awarded you will receive an email prompt to activate your `E*Trade` account, until that time no action is required.
1. [ ] Read about the [Employee Stock Purchase Plan (ESPP)](https://internal-handbook.gitlab.io/handbook/total-rewards/employee-stock-purchase-program/) at GitLab. The ESPP is a benefit GitLab offers that allows employees to optionally purchase shares of GitLab stock at a discount through regular payroll deductions over specified Offering Periods. If eligible, you will receive an email to participate in the ESPP once the enrollment period is open.
1. [ ] Take the Following Assessments (remember any questions you may have around these can be routed to the [`#people-connect`](https://gitlab.slack.com/messages/people-connect) Slack channel):
   - [ ] [GitLab Compensation Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/compensation/#knowledge-assessment)
   - [ ] [GitLab Benefits Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/benefits/#knowledge-assessment)


### Payroll & Expensing
1. [ ] Take a look at the [Accounts Payable](https://about.gitlab.com/handbook/finance/accounts-payable/) page for information on how to expense items and sending invoices
1. [ ] Keep track of your expenses in Expensify (via Okta). After you enter your expense, don't forget to send the expense report to your manager for approval. If you have any questions on submitting an expense report ask in the [`#expense-reporting-inquires`](https://gitlab.slack.com/messages/expense-reporting-inquires) slack channel. Depending on your location, the finance team may ask you to complete additional steps to receive reimbursement.
1. [ ] Take a look at the [Payroll](https://about.gitlab.com/handbook/finance/payroll/) page for more information on paychecks.

### Travel & Contribute
1. [ ] Check the [GitLab Contribute handbook page](https://about.gitlab.com/company/culture/contribute/) to get familiar with the GitLab Contribute event.
1. [ ] View the `Card 4.19.17.pdf` located [here](https://drive.google.com/file/d/0B4eFM43gu7VPOHlLWFNBV1BZVE0tZ2NDVDJQd0hBWVotUGIw/view?usp=sharing&resourcekey=0-rruqO9weBMej2mDaDhBFZg) and save your [Traveler Insurance ID card](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy) located on page 1.
1. [ ] We use TripActions for company travel; please [create your account through Okta](https://gitlab.okta.com/app/UserHome). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to set up your account.

### Your Role
- Spend the rest of your day focused on role-based tasks, some of which you will find at the bottom of this issue, others can be found in your role-based training issue which your manager will have opened and assigned to you.

</details>

---

### Day 5 - Git

**Welcome to day 5! Today you will start learning more about Git and how we use it in the course of everyday work at GitLab. Do not be intimidated, we have some great resources for learning. Please do keep in mind that it may take you longer than a day to complete this section, that is okay!**

<details>
<summary>New Team Member</summary>

For those new to Git and GitLab, it's important to get familiar with how Git works. You don't need to become a GitLab expert in a day, but understanding the below is a key reference as you acclimate. Be sure to reach out to your onboarding buddy, manager, or any team member at GitLab for Git-related questions and help.

##### Helpful Slack Channels for Git Questions
- Any help with Merge Requests: [`#mr-buddies`](https://gitlab.slack.com/messages/mr-buddies) and the handbook about [Merge Request Buddies](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/)
- Any general help with Git: [`#questions`](https://gitlab.slack.com/archives/questions)
- Specific questions using Git in the terminal: [`#git-help`](https://gitlab.slack.com/archives/git-help)

##### Introduction to Git and GitLab - SSH Access (familiar with Git)

1. [ ] If you have never used Git before, we **recommend** you skip this step and use the [WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/) to complete any Git-related onboarding tasks. The WebIDE doesn't require you to set or use any command-line tools.
   * If you plan to use Git on your local machine then you'll need to [add an SSH key to your GitLab profile](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account). The SSH key provides security and allows you to authenticate to the GitLab remote server without supplying your username or password each time. Please note that following best practices, you should always favor ED25519 SSH keys, since they are more secure and have better performance over the other types.
   * Alternatively, you can clone over HTTPS. You'll need to generate a [personal access token](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) as account password with 2FA enabled. This method helps when SSH is blocked in your environment (e.g. hotel wifis).
   * Some GitLab projects (for example [Version App](https://gitlab.com/gitlab-services/version-gitlab-com#contributing)) require commits to be signed with GPG. Consider [setting up GPG Key](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#generating-a-gpg-key).

##### Introduction to Git and GitLab - General Breakdown

1. [ ] Complete the [GitLab Team Members Certification](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-team-members-certification). This certification will take approximately 6 hours to complete and will teach the fundamentals of using GitLab for team members. Consider blocking off time during your onboarding to complete. Complete the training within seven days of creating the account in GitLab Demo Cloud because the account will expire by then.
1. [ ] Become familiar with how Git works by watching this [Git-ing started with Git video.](https://www.youtube.com/watch?v=Ce5nz5n41z4)


##### Merge Requests

Note: Key points within the GitLab Team Members Certification for Merge Requests.

1. [ ] When you are first starting at GitLab, you can assign your merge requests (MRs) to your manager but going forward, it's best practice to assign the MR to the relevant department. 
1. [ ] Make iterations quickly and efficiently. Do not wait until you are done before creating an issue or merge request.
   - `Draft:` is what you prepend to a merge request in GitLab in order to prevent your work from being merged if it still needs more work.
1. [ ] Review our [Practical Handbook Edit Examples](https://about.gitlab.com/handbook/practical-handbook-edits/) to learn more about creating new handbook pages.
1. [ ] Learn more about GitLab quick actions and keyboard shortcuts for issues and merge requests in [this blog post on productive workflows](https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/)

It is highly encouraged to complete the [GitLab Team Members Certification](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/gitlab-team-members-certification) before moving onto the **[Team Page](https://about.gitlab.com/company/team/)** steps.

##### Adding Yourself to the [Team Page](https://about.gitlab.com/company/team/)

Please be aware that this task is a key learning opportunity and strongly recommended for all team members. This exercise is an important step as a GitLab team member and is a critical first example of learning our product firsthand, in a personalized way.

1. [ ] Update your photo, social media handles and story on the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). For the safety of our team members, starting 2022-03-03, `Locality` should be left as empty and `Country` should be set to `Remote`. 
   * If you are a manager, your slug will need to match the `reports_to` line for the people on your team. If you update your slug you should update `reports_to` for your team members as well.
   * If you are in the Sales organization, please consider adding your Sales region.
   * Please make sure to include your nickname if you prefer to be called anything other than your first name. If you do not feel comfortable with your full name on the team page, please change it to what feels appropriate to you.
1. [ ] If you have a pet, please also consider adding them to the [team pets page](https://about.gitlab.com/team-pets/) by following the instructions on the [same page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). Please ensure to add your first and last name and/or GitLab handle so that we can identify whose pets belong to whom.

**Please note in the current YouTube video, at 10:17 mark, you will need to use the `sites/uncategorized/` rather than `sites/marketing/`**

#### Access Requests

At GitLab, anytime you need access to a system that has not already been provisioned in your new hire Access Request, a new Access Request will need to be created. 

1. [ ] Review the [GitLab Access Request process](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/).
1. [ ] We have a [Tech Stack file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) that will help guide you to see who the provisioners are for a specific system.

#### Weeks 2-4 Reminders
- [ ] Schedule work blocks over the next 30 days on your calendar to complete the tasks under Weeks 2 - 4 below. [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays) are a great time to come back to this issue.

### Your Role
- Spend the rest of your day focused on role-based tasks, some of which you will find at the bottom of this issue, others can be found in your role-based training issue which your manager will have opened and assigned to you.

#### Happy Dance

1. [ ] Do a little happy dance. You just finished your first week at GitLab!

**Week one was focused on compliance tasks, getting your systems and tools in place, enrolling in benefits, and learning more about life at GitLab. Next week which is of equal importance, you will take a deeper dive into our core values and the handbook while learning about the importance of Diversity, Inclusion, and Belonging (DIB) here at GitLab.**

</details>

---

### Weeks 2 - 4 : Explore <!-- Intern: Adjust to second week -->

**Welcome to week two :tada: This week really emphasizes the importance of Diversity, Inclusion, and Belonging (DIB). As one of our core CREDIT Values this is something place great importance on here at GitLab. This week should also be used to continuing to read the handbook, learning how to use Git, and getting to know your peers.**

<details>
<summary>New Team Member</summary>

1. [ ]  Be sure to spend equal time on this critical onboarding issue, as well as your role-based onboarding and training.
1. [ ] :red_circle: Put some time aside to uncover new information in the team [handbook](https://about.gitlab.com/handbook/). A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. This is NOT a traditional company handbook of rules and regulations. It is a living document that is public to the world and constantly evolving. Absolutely anyone, including you, can suggest improvements or changes to the handbook! It's [encouraged](https://about.gitlab.com/handbook/handbook-usage/)! The handbook may seem overwhelming, but don't let it scare you. To simplify navigating the handbook, here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.
   - [ ] Read [about](https://about.gitlab.com/about/) the company, and [how we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](https://about.gitlab.com/culture/), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).

#### Diversity, Inclusion & Belonging
1. [ ] Understanding that Diversity, Inclusion & Belonging is one of GitLab's top priorities please complete GitLab's [Diversity, Inclusion & Belonging certification](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/dib-training-certification)
1. [ ] The following resources were covered in the DIB certification above. Please acknowledge that you have gone through these pages. **Note:** You do not need to re-read.
   - [ ] GitLab's [Diversity, Inclusion & Belonging page](https://about.gitlab.com/company/culture/inclusion/)
   - [ ] GitLab's [Diversity, Inclusion & Belonging Events](https://about.gitlab.com/company/culture/inclusion/diversity-and-inclusion-events/)
   - [ ] GitLab's [Diversity, Inclusion & Belonging trainings](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities)
   - [ ] GitLab's [Building an Inclusive Remote Culture page](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/)
   - [ ] Optionally, you can contribute by collaborating or creating a new [issue](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues)

#### Learning and Development opportunities for team members

- [ ] Review Learning and Development [resources for team members](https://about.gitlab.com/handbook/people-group/learning-and-development/#team-member-resources) and think about how you want to grow your career and skills as a member of the GitLab team.

#### Objective, Key, Results (OKRs)

- [ ] Review our [OKR page](https://about.gitlab.com/company/okrs/). Take time to understand how OKRs work at GitLab and why we use them.

#### Talent Development Program

We want all our team members to be successful! We continuously strive to maintain a high standard of performance across the entire company.
GitLab supports team members who wish to continue their education and growth within their professional career. We also facilitate regular career development conversations to ensure we have transparency and ongoing feedback between team members and their managers.

- [ ] Read about GitLab's [Growth and Development Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/)
- [ ] Familarilize yourself with [GitLab's Talent Assesment Program](https://about.gitlab.com/handbook/people-group/talent-assessment/) 

#### Additional Product Information
1. [ ] GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
1. [ ] Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
1. [ ] Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/#three-tiers) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motivation behind each tier.
1. [ ] Review the [Buyer Persona handbook page](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/) and materials. 

#### Surveys and Reviews
1. [ ] In the interests of paying it forward and contributing to our core values of [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) - you will receive an [Onboarding Satisfaction Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) after your first month as a GitLab team member. We encourage you to candidly share your feedback and suggestions based on your onboarding experience, this will also serve as an opportunity to acknowledge your Onboarding Buddy and ensure they are included in the [Quarterly Onboarding Buddy Raffle](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#buddy-program).
1. [ ] Consider creating a video sharing your experience of onboarding using the guidelines documented [here](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/) this is a great way to support future joiners in acclimating to GitLab.
1. [ ] Help spread the word about life at GitLab. Set a reminder in sixty days to share your feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab). We welcome all feedback.

</details>

### Job-specific tasks

---
<!-- include: role_tasks -->

<!-- include: people_manager_tasks -->

<!-- include: division_tasks -->

<!-- include: intern_tasks -->

---

<!-- include: department_tasks -->

---

<!-- Labels -->
/label ~"onboarding"

/label ~"PE:: Pending Pre-Start" 

/label ~"Manager:: Pre Start Pending" 

/label ~"MGR:: Day 1 - Pending"
