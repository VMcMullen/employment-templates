Hello {{CANDIDATE_FIRST_NAME}},

You are all set to become a new GitLab employee, or team member as we call ourselves at GitLab. 

This is our last official communication before I hand you over to the People Experience Associate team for onboarding. It has been a pleasure and I hope we get to have a coffee chat in the near future! 

A few things to note for your first day:

**What time will you receive your access?**
The People Experience team in charge of onboarding currently has several team members, and they schedule your access emails to arrive to you in your morning, usually your early morning. They know your timezone, most of the time the access email will be sent to your inbox at 6:00 am your local time. The most important fact to remember is that you will have your email waiting in your personal inbox for whenever you decide to start your day, and you are free to start your day at any time that is most convenient for you or prearranged with your manager.

In advance of you waiting to receive your access email check out this helpful [link](https://about.gitlab.com/handbook/general-onboarding) on how we structure your onboarding. On this page, you will be able to select the relevant department that you will join and then see an onboarding issue example. At GitLab, we discuss projects and keep checklists in documents called issues. You can also think of this as your onboarding checklist. Don't feel overwhelmed! The onboarding issue is meant to be a guide for completing your onboarding items during your first 30 days at GitLab, not during the first week. 

**How many emails will you receive?**
You will receive the following emails:
1. Welcome to your GitLab Onboarding
1. "{{CANDIDATE_FIRST_NAME}}, your Google Account password has been reset by your administrator"

**What should you complete first?**
It is extremely important that you open and read the email titled "Welcome to your GitLab Onboarding" first. Please read the entire email and watch the video attached, which walks you through how to proceed with your account set up.

Following the video and directions in your Welcome to Onboarding email, you will be taken to your ONBOARDING ISSUE. When you have access to your Onboarding Issue, please go to Day 1: Accounts and Paperwork and find New Team Member. Complete those tasks in order.

**What if you have access difficulties?**
If you have issues logging in your morning, please email the onboarding team at peopleops@gitlab.com. If you have issues regarding your onboarding issue while you are there, please comment at the bottom and tag your appropriate PeopleOps team member. If you have technical questions, they have Slack channels listed in the issue.

We look forward to welcoming you soon!

Best,

{{MY_FULL_NAME}}
